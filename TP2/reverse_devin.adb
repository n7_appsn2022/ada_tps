-- Callune Gitenet SN APP 1A
-- L'utilisateur a choisi un nombre entre 1 et 100 et la machine doit deviner ce nombre. A chaque proposition de la machine l'utilisateur repond par '<', '>' ou '=' suivant que son nombre soit plus petit, plus grand ou egal.
-- précondition : 
-- postcondition : On joue au jeu du devin 
-- Tests : L'utilisateur choisi 66 | Machine -> 50 | > | Machine -> 75 | < | Machine -> 62 | > | Machine -> 68 | < | Machine -> 65 | > | Machine -> 66 | = | Machine -> Chouette, je le trouve a tous les coups ! Nombre d'essais : 6


-- with use block
with alea;
use alea;

with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io ;

procedure reverse_devin is

	n     : integer; -- Nombre devine par la machine
	essai : integer; -- Nombre d'essais
	rep   : character; -- Reponse de l'utilisateur
	debut : integer; -- Borne de debut de l'intervalle du nombre a deviner
	fin   : integer; -- Borne de fin de l'intervalle du nombre a deviner

begin
	
	essai := 0;
	debut := 1;
	fin := 100;
	
	loop
	
		-- R1 : La machine fait une recherche par dichotomie pour deviner le nombre
		-- R2 : Choix du nombre entre debut et fin
		n := (debut + fin) / 2;
		
		-- R1 : Affichage du nombre devine par la machine
		put_line(n);
		
		-- R1 : On lit la reponse de l'utilisateur 
		while rep /= '<' and rep /= '>' and rep /= '=' loop
			put_line("Reponse ? (<, > ou =)");
			get(rep);
        exit when rep = '<' or rep = '>' or rep = '='
		end loop;
		
		-- { debut <= nombre de l'utilisateur <= fin }
		-- R1 : La machine change ses bornes debut et fin suivant la reponse de l'utilisateur
		-- R2 : Si le nombre de l'utilisateur est plus petit que celui de la machine, la borne de fin est egal au nombre propose par la machine (n)
		if rep = '<' then
			fin := n;
		-- R2 : Si le nombre de l'utilisateur est plus grand que celui de la machine, la borne de debut est egal au nombre propose par la machine (n)
		elsif rep = '>' then
			debut := n;
		else
			null;
		end if;	
		-- { debut <= nombre de l'utilisateur <= fin }
		
		-- R1 : On incremente le nombre d'essais
		essai := essai + 1;
	
	exit when rep = '='; -- R1 : Jusqu'a ce que rep = '='
	end loop;
	
	-- R1 : Affichage du message de fin et du nombre d'essais
	put("J'ai gagné ! Nombre d'essais :");
	put(essai);

end reverse_devin;