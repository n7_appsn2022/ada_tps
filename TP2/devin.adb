-- Callune Gitenet SN APP 1A
-- Ecrire un programme dans lequel la machine choisit un nombre aleatoirement entre 1 et 100 et l'utilisateur doit le deviner en entrant un entier au clavier. Le nombre de propositions est affiche en fin de partie.
-- précondition : 
-- postcondition : On joue au jeu du devin 
-- test :  n = 71 | Saisissez un nombre -> 50 -> C'est plus -> 80 -> C'est moins -> 71 -> Gagne ! Nombre d'essais : 3
--	n = 18 | Saisissez un nombre -> -50 -> C'est plus -> 123654 -> C'est moins -> 18 -> Gagne ! Nombre d'essais : 3

-- with use block

with alea;
use alea;

with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io ;

procedure devin is

	n_alea : integer; -- Le nombre aleatoire a deviner
	nb_uti : integer; -- Le nombre que l'utilisateur va choisir
	essai  : integer; -- Le nombre d'essais fait par l'utilisateur

begin
	n_alea := alea_1_100;
	-- { 1 <= n <= 100 }
	essai := 0;
	
	loop 
	-- R1 : L'utilisateur saisit un nombre entre 1 et 100
	loop
        put_line("Quel est le nombre entre 1 et 100 que j'ai choisit ?");
        Get(nb_uti);
    exit when nb_uti>0 and nb_uti< 101;
    end loop;
	
	-- R1 : On affiche "C'est plus" ou "C'est moins" selon le nombre de l'utilisateur
	if nb_uti < n_alea then
		put_line("plus");
	elsif nb_uti > n_alea then
		put_line("moins");
	else
		null;
	end if;
	
	-- R1 : On incremente le nombre d'essais
	essai := essai + 1;
	
	exit when n_alea = nb_uti; -- R1 : Jusqu'a ce que l'utilisateur trouve le bon nombre
	end loop;
	-- { n_alea = nb_uti }
	-- { essai >= 1 }
	
	-- R1 : Affichage du nombre d'essais
	put("Gagné ! Nombre d'essais :");
	put(essai);

end devin;