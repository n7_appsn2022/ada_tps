-- Callune Gitenet SN APP 1A
-- Piloter un drone par un menu qui permet de le demarrer, le monter, le descendre ou quitter.
-- précondition : 
-- postcondition : On joue au jeu du devin 
-- test : 
--	Que faire : (d)emarrer (m)onter de(s)cendre (q)uitter => d : Drone allume
-- 															 m : Altitude : 1
-- 															 q : Au revoir

-- with use block

with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io ;

procedure drone is

	c 		: Character;-- Choix de l'utilisateur
	is_on 	: Boolean; 	-- Vrai si le drone est allume, faux sinon
	alt		: Integer; 	-- Altitude du drone
	
begin

	is_on := false;
	alt := 0;
	
	loop
	
		-- R1 : Saisir d, m, s, q pour le choix du menu
		loop
			put_line("Que faire : (d)emarrer (m)onter de(s)cendre (q)uitter");
			get(c);
		exit when c = 'd' or c = 'm' or c = 's' or c = 'q';
		end loop;
		-- { c = {d, m, s, q} }
	
		-- R1 : Traiter la demande
		case c is
			-- R2 : Si c = d, on démarre le drone
			when 'd' =>
				-- Si on demarre le drone une nouvelle fois, il reste allume
				is_on := true; 
				put_line("Drone allume");
			-- R2 : Si c = m, on s'occupe de faire monter le drone
			when 'm' =>
				-- R3 : Si le drone est allume, on le fait monter et on affiche son altitude
				if is_on then
					alt := alt + 1;
					put("Altitude : ");
					put(alt);
					put_line("");
				-- R3 : Sinon on ne fait rien (on informe que le drone est eteint)
				else
					put_line("Le drone doit etre allume pour monter");
				end if;
			-- R2 : Si c = s, on s'occupe de faire descendre le drone
			when 's' =>
				-- R3 : Si le drone est allume et est deja en hauteur, on le fait descendre et on affiche son altitude
				if is_on and alt > 0 then
					alt := alt - 1;
					put("Altitude : ");
					put(alt);
					put_line("");
				-- R3 : Sinon on ne fait rien
				else
					put_line("Le drone ne peut pas descendre en altitude negative");
				end if;
			-- R2 : Si c = q, on arrete de jouer avec le drone (on dit au revoir)
			when 'q' => 
				put_line("Au revoir");
			-- R2 : Sinon, on ne fait rien
			when others => 
				null;
		end case;
		
	exit when c = 'q' or alt >= 5; -- R1 : On quitte lorsque l'utilisateur veut quitter ou que le drone a atteint une altitude d'au moins 5
	end loop;
	-- { 0 <= alt <= 5}

end drone;