-- Callune Gitenet SN APP 1A
-- Joue au jeu de la vie
-- précondition : 
-- postcondition : 
-- test :  

-- with use block
procedure jeu_de_la_vie is

-- constante declaration
-- types declaration
-- procedure & function block
-- main's variable

-- R1.1 Initialisation
-- out : nbColonne entier >0, nbLigne entier >0, genActuelle T_cellule[nbLigne][nbColonne],isModified bollean , nbGeneration entier >0, genSuivante T_cellule[nbLigne][nbColonne]

-- R1.2 On joue jusqu'a ce que ça ne bouge plus, ou que le nombre de génération soit terminer 
-- in :  isModified bollean , nbGeneration entier >0

    -- R2.1 de R1.2 Parcourir le tableau 
    -- in: nbColonne entier >0, nbLigne entier >0, genActuelle T_cellule[nbLigne][nbColonne] 
    -- out: colonne entier entre [1;nbColonne], ligne entier entre [1;nbLigne] 

        -- R3.1 de R2.1 Compter les voisins vivants de la cellule courante
        -- in: colonne,ligne,GenActuelle
        -- out: nbVoisinVivant entier entre [0;8]

        -- R3.2 de R2.1 Calculer l'état suivant de la cellule courante
        -- in: nbVoisinVivant , etatCellCourante T_Enum_Cell 
        -- out: etatSuivant T_Enum_Cell, isModified

        -- R3.3 de R2.1 Inscrire l'état suivant dans la génération suivante
        -- in: etatSuivant,colone,ligne
        -- in/out: genSuivante (seule la case ligne,colone est modifié)

    -- R2.2 de R1.2 Passer à la génération suivante
    -- in: genSuivante
    -- out: genActuelle
    



end jeu_de_la_vie;

 