-- Callune Gitenet SN APP 1A
-- utilise un package
-- with use block
with p_liste;

with text_io;
use text_io;



procedure main is
    procedure Put_Int(e : in Integer) is
    begin
        Put(Integer'Image(e));
    end Put_Int;
    
    procedure Put_Chaine(e : in Character) is
    begin
        Put(e);
    end Put_Chaine;
        

    package p_liste_entier is new p_liste(T_generic => Integer, afficher_element => Put_Int);
    use p_liste_entier;

    package p_liste_str is new p_liste(T_generic => Character, afficher_element => Put_Chaine);
    use p_liste_str;

    -- constante declaration
    -- exception declaration
    -- types declaration
    -- procedure & function block
    -- main's variable
    my_list_int : p_liste_entier.t_liste;
    my_list_str : p_liste_str.t_liste;

begin 
    my_list_int := creer_liste_vide;
    afficher_liste(my_list_int);
    inserer_en_tete(my_list_int,2);
    inserer_en_tete(my_list_int,5);
    afficher_liste(my_list_int);
    inserer_apres(my_list_int,3,2);
    afficher_liste(my_list_int);
    inserer_avant(my_list_int,1,2);
    afficher_liste(my_list_int);
    enlever(my_list_int,2);
    afficher_liste(my_list_int);


    my_list_str := p_liste_str.creer_liste_vide;
    p_liste_str.afficher_liste(my_list_str);
    p_liste_str.inserer_en_tete(my_list_str,'e');
    p_liste_str.inserer_en_tete(my_list_str,'a');
    p_liste_str.afficher_liste(my_list_str);
    p_liste_str.inserer_apres(my_list_str,'t','e');
    p_liste_str.afficher_liste(my_list_str);
    p_liste_str.inserer_avant(my_list_str,'k','t');
    p_liste_str.afficher_liste(my_list_str);
    p_liste_str.enlever(my_list_str,'e');
    p_liste_str.afficher_liste(my_list_str);
end main;