-- Callune Gitenet SN APP 1A
-- implementation d'une liste chainée d'T_generic
-- with use block
generic
    type T_generic is private; 
    with procedure afficher_element(element:in T_generic);
package p_liste is
    

-- constante declaration
-- exception declaration
data_not_in_liste : exception;
empty_liste :exception;
-- types declaration
Type t_cell_integer is private;
type t_liste  is private;

-- procedure & function block

    -- nom : créer_liste_vide
    -- sémantique : créer_liste_vide
    -- type de retour : t_liste
    -- paramètres : 
    -- pré-condition : 
    -- post-condition : est vide
    -- exception : 
    -- test :  

    function creer_liste_vide return t_liste ;

    -- nom : est_vide
    -- sémantique : est_vide
    -- type de retour : boolean
    -- paramètres :  F_liste : in t_liste
    -- pré-condition : 
    -- post-condition : est vide
    -- exception : 
    -- test :  

    function est_vide (F_liste: in t_liste) return boolean ;

    -- nom :inserer_en_tete
    -- sémantique : inserer_en_tete
    -- paramètres : F_liste: In/Out liste 
    --              F_nouveau : In T_generic 
    -- pré-condition : 
    -- post-condition : F_nouveau apartien F_liste
    -- exception : 
    -- test :   
    procedure inserer_en_tete (F_liste : in out t_liste;F_nouveau : in T_generic ) ;

    -- nom :afficher_liste
    -- sémantique : afficher_liste
    -- paramètres : F_liste: In/Out liste 
    -- pré-condition : 
    -- post-condition : 
    -- exception : 
    -- test :   
    procedure afficher_liste (F_liste : in out t_liste) ;

    -- nom : rechercher
    -- sémantique : rechercher un element dans la liste retourne son adresse ou null
    -- type de retour : t_liste
    -- paramètres :  F_liste : in t_liste
    --               F_element : in T_generic
    -- pré-condition : 
    -- post-condition : 
    -- exception : 
    -- test :  

    function rechercher (F_liste: in t_liste; F_element :in T_generic) return t_liste ;

    -- nom :inserer_apres
    -- sémantique : insere F_nouveau dans F_liste juste après F_data
    -- paramètres : F_liste: In/Out liste 
    --              F_nouveau : in T_generic
    --              F_data : in T_generic
    -- pré-condition : 
    -- post-condition : F_nouveau appartion a F_liste
    -- exception :  data n'est pas dans la liste
    --              laend p_liste_entier;
    -- test :   
    procedure inserer_apres (F_liste : in out t_liste; F_nouveau:in T_generic; F_data: in T_generic);

    -- nom :inserer_avant
    -- sémantique : insere F_nouveau dans F_liste juste avant F_data
    -- paramètres : F_liste: In/Out liste 
    --              F_nouveau : in T_generic
    --              F_data : in T_generic
    -- pré-condition : 
    -- post-condition : F_nouveau appartion a F_liste
    -- exception :  data n'est pas dans la liste
    --              la liste est vide
    -- test :   
    procedure inserer_avant (F_liste : in out t_liste; F_nouveau:in T_generic; F_data: in T_generic);

    -- nom :enlever
    -- sémantique : enlever F_element de F_liste 
    -- paramètres : F_liste: In/Out liste 
    --              F_element : in T_generic
    -- pré-condition : 
    -- post-condition : F_element n'appartion pas a F_liste
    -- exception : 
    -- test :   
    procedure enlever (F_liste : in out t_liste; F_element:in T_generic);
private 
    -- types declaration
    type t_liste  is access t_cell_integer;
    type t_cell_integer is record
            element : T_generic;
            suivant : t_liste;
    end record;

end p_liste;