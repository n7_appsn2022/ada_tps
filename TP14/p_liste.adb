-- Callune Gitenet SN APP 1A
-- implementation d'une liste chainée d'T_generic
-- with use block
with text_io;
use text_io;

package body  p_liste is
-- procedure & function block

    -- nom : créer_liste_vide
    -- sémantique : créer_liste_vide
    -- type de retour : t_liste
    -- paramètres : 
    -- pré-condition : 
    -- post-condition : est vide
    -- exception : 
    -- test :  

    function creer_liste_vide return t_liste is
    --déclaration variables locales
    new_liste :t_liste;
    begin
        new_liste := null;
        return new_liste;
    end creer_liste_vide;

    -- nom : est_vide
    -- sémantique : est_vide
    -- type de retour : boolean
    -- paramètres :  F_liste : in t_liste
    -- pré-condition : 
    -- post-condition : est vide
    -- exception : 
    -- test :  

    function est_vide (F_liste: in t_liste) return boolean is
    --déclaration variables locales
    begin
        return F_liste = null;
    end est_vide;

    -- nom :inserer_en_tete
    -- sémantique : inserer_en_tete
    -- paramètres : F_liste: In/Out liste 
    --              F_nouveau : In T_generic 
    -- pré-condition : 
    -- post-condition : F_nouveau apartien F_liste
    -- exception : 
    -- test :   
    procedure inserer_en_tete (
        F_liste : in out t_liste;
        F_nouveau : in T_generic ) is
    -- déclaration variables locales
    newliste : t_liste;
    begin
        if F_liste = null then
            F_liste := new t_cell_integer;
            F_liste.all.element := F_nouveau;
            F_liste.all.suivant := null;
        else
            newliste := new t_cell_integer;
            newliste.all.element := F_nouveau;
            newliste.all.suivant := F_liste;
            F_liste := newliste;
        end if;
    end inserer_en_tete;

    -- nom :afficher_liste
    -- sémantique : afficher_liste
    -- paramètres : F_liste: In/Out liste 
    -- pré-condition : 
    -- post-condition : 
    -- exception : 
    -- test :   
    procedure afficher_liste (F_liste : in out t_liste) is
    -- déclaration variables locales
    begin
        if F_liste /= null then
            afficher_element(F_liste.all.element);
            afficher_liste(F_liste.all.suivant);
        end if;
        new_line;
    end afficher_liste;

    -- nom : rechercher
    -- sémantique : rechercher un element dans la liste retourne son adresse ou null
    -- type de retour : t_liste
    -- paramètres :  F_liste : in t_liste
    --               F_element : in T_generic
    -- pré-condition : 
    -- post-condition : 
    -- exception : 
    -- test :  

    function rechercher (F_liste: in t_liste; F_element :in T_generic) return t_liste is
    --déclaration variables locales
    trouve : boolean;
    iterator : t_liste;
    begin
    trouve := false;
    iterator := F_liste;
        while iterator /= null and not trouve loop 
            if iterator.all.element = F_element then
                trouve := true;
            else
                iterator:= iterator.all.suivant;
            end if;
        end loop;
        return iterator;
    end rechercher;

    -- nom :inserer_apres
    -- sémantique : insere F_nouveau dans F_liste juste après F_data
    -- paramètres : F_liste: In/Out liste 
    --              F_nouveau : in T_generic
    --              F_data : in T_generic
    -- pré-condition : 
    -- post-condition : F_nouveau appartion a F_liste
    -- exception :  data n'est pas dans la liste
    --              la liste est vide
    -- test :   
    procedure inserer_apres (F_liste : in out t_liste; F_nouveau:in T_generic; F_data: in T_generic) is
    -- déclaration variables locales
    iterator : t_liste;
    tmp : t_liste;
    begin
        if est_vide(F_liste) then
            raise empty_liste;
        elsif rechercher(F_liste,F_data) = null then 
            raise data_not_in_liste;
        else
            iterator := F_liste;
            while iterator.all.element /= F_data loop
                iterator := iterator.all.suivant;
            end loop;
            tmp := iterator.all.suivant;
            iterator.all.suivant := new t_cell_integer;
            iterator.all.suivant.all.element := F_nouveau;
            iterator.all.suivant.all.suivant := tmp;
        end if;
    end inserer_apres;

    -- nom :inserer_avant
    -- sémantique : insere F_nouveau dans F_liste juste avant F_data
    -- paramètres : F_liste: In/Out liste 
    --              F_nouveau : in T_generic
    --              F_data : in T_generic
    -- pré-condition : 
    -- post-condition : F_nouveau appartion a F_liste
    -- exception :  data n'est pas dans la liste
    --              la liste est vide
    -- test :   
    procedure inserer_avant (F_liste : in out t_liste; F_nouveau:in T_generic; F_data: in T_generic) is
    -- déclaration variables locales
    iterator : t_liste;
    tmp : t_liste;
    begin
        if est_vide(F_liste) then
            raise empty_liste;
        elsif rechercher(F_liste,F_data) = null then 
            raise data_not_in_liste;
        else
            iterator := F_liste;
            if iterator.all.element = F_data then 
                inserer_en_tete(F_liste,F_nouveau);
            else 
                while iterator.all.suivant.all.element /= F_data loop
                    iterator := iterator.all.suivant;
                end loop;
                tmp := iterator.all.suivant;
                iterator.all.suivant := new t_cell_integer;
                iterator.all.suivant.all.element := F_nouveau;
                iterator.all.suivant.all.suivant := tmp;
            
            end if;
        end if;
    end inserer_avant;

    -- nom :enlever
    -- sémantique : enlever F_element de F_liste 
    -- paramètres : F_liste: In/Out liste 
    --              F_element : in T_generic
    -- pré-condition : 
    -- post-condition : F_element n'appartion pas a F_liste
    -- exception : 
    -- test :   
    procedure enlever (F_liste : in out t_liste; F_element:in T_generic) is
    -- déclaration variables locales
    iterator : t_liste;
    begin
    iterator := F_liste;
        while iterator /= null loop 
            if iterator.all.element = F_element then
                iterator.all:= iterator.all.suivant.all;
            else
                iterator:= iterator.all.suivant;
            end if;
        end loop;
    end enlever;
end p_liste;