-- Callune Gitenet SN APP 1A
-- utilise un package
-- with use block
with p_calculatrice;
use p_calculatrice;

with text_io;
use text_io;

procedure main is
    -- instanciation de la gé&néricité 
    -- constante declaration
    -- exception declaration
    -- types declaration
    -- procedure & function block
   commande : String(1..10);
   longueur : Integer;   
   com : character ;
   quitter : boolean;
   my_calculator : t_calculator;
begin 
   my_calculator:= p_calculatrice.init;
   quitter := false;
   loop
      put("Veuillez saisir un opérande ou une opération ou (r)eset ou (q)uitter: ");
      get_line(commande, longueur);
      com := commande(1);
      begin
         case com is
            when '+' => p_calculatrice.add(my_calculator) ;
            when '-' => p_calculatrice.sub(my_calculator) ;
            when '*' => p_calculatrice.mul(my_calculator) ;
            when '/' => p_calculatrice.div(my_calculator) ;
            when 'r' => p_calculatrice.reset(my_calculator) ;
            when 'q' => quitter := true ;
            when others   => p_calculatrice.push(my_calculator,Integer'Value(commande(1..longueur))) ;
         end case;
      exception
         when zero_devide_exception => put_line("Division par zéro impossible");
         when Constraint_Error => put_line("Saisie invalide.");
         when not_enougth_operande => put_line("Pas assez d'opérandes, saisir plus de chiffre.");
      end;
      new_line;
   exit when quitter;
   end loop;
end main;