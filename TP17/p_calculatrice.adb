-- Callune Gitenet SN APP 1A
-- paquet qui implemente la calculatrice polonaise

package body p_calculatrice is
    -- constante declaration
    -- exception declaration
    -- types declaration
    -- procedure & function block

        -- nom : init
        -- sémantique : initialize the calculator
        -- type de retour : t_stack
        -- paramètres : 
        -- pré-condition : 
        -- post-condition : isEmpty
        -- exception : 
        -- test :  

        function init return t_calculator is 
            new_calculator : t_calculator;
        begin 
            new_calculator.stack := p_pile_entier.create_empty_stack ;
            new_calculator.nb_elements := 0;
            return new_calculator;
        end init;

        -- nom : reset
        -- sémantique : reset
        -- paramètres :  F_calculator : in out t_calculator
        -- pré-condition : 
        -- post-condition : 
        -- exception : 
        -- test :  

        procedure reset (F_calculator: in out t_calculator) is
        begin
            F_calculator := init;
        end reset;

        -- nom : push
        -- sémantique : stack number on top 
        -- paramètres : F_calculator: In/Out t_calculator 
        --              F_nouveau : In Integer 
        -- pré-condition : 
        -- post-condition : F_nouveau apartien F_calculator
        -- exception : 
        -- test :   
        procedure push (F_calculator : in out t_calculator; F_nouveau : in Integer ) is 
        begin
            p_pile_entier.push(F_calculator.stack, F_nouveau);
            F_calculator.nb_elements := F_calculator.nb_elements +1;
        end push;

        -- nom : pop
        -- return Integer
        -- sémantique : pop number on top 
        -- paramètres : F_calculator: In/Out t_calculator 
        -- pré-condition : 
        -- post-condition : F_calculator.nbeleme = nbelem -1
        -- exception : 
        -- test :   
        function pop (F_calculator : in out t_calculator) return Integer is 
        begin 
            if F_calculator.nb_elements < 1 then
                raise not_enougth_operande ;
            else
                F_calculator.nb_elements := F_calculator.nb_elements -1;
                return pop(F_calculator);
            end if;
        end pop;

        -- nom : add
        -- sémantique : take the 2 first element of the stack and perform addition, then put the result in the stack
        -- paramètres : F_calculator: In out t_calculator 
        -- pré-condition : 
        -- post-condition : nb_elements -1
        -- exception : not_enougth_operande when nb_elements <2
        -- test :   
        procedure add (F_calculator : in out t_calculator) is
            operande1 : Integer;
            operande2 : Integer;
        begin
            if F_calculator.nb_elements < 2 then
                raise not_enougth_operande ;
            else
                operande1 := pop(F_calculator);
                operande2 := pop(F_calculator);
                push(F_calculator , operande1 + operande2);
            end if;
        end add;

        -- nom : sub
        -- sémantique : take the 2 first element of the stack and perform substraction, then put the result in the stack
        -- paramètres : F_calculator: In out t_calculator 
        -- pré-condition : 
        -- post-condition : nb_elements -1
        -- exception : not_enougth_operande when nb_elements <2
        -- test :   
        procedure sub (F_calculator : in out t_calculator) is
            operande1 : Integer;
            operande2 : Integer;
        begin
            if F_calculator.nb_elements < 2 then
                raise not_enougth_operande ;
            else
                operande1 := pop(F_calculator);
                operande2 := pop(F_calculator);
                push(F_calculator , operande1 - operande2);
            end if;
        end sub;

        -- nom : mul
        -- sémantique : take the 2 first element of the stack and perform multiplication, then put the result in the stack
        -- paramètres : F_calculator: In out t_calculator 
        -- pré-condition : 
        -- post-condition : nb_elements -1
        -- exception : not_enougth_operande when nb_elements <2
        -- test :   
        procedure mul (F_calculator : in out t_calculator) is
            operande1 : Integer;
            operande2 : Integer;
        begin
            if F_calculator.nb_elements < 2 then
                raise not_enougth_operande ;
            else
                operande1 := pop(F_calculator);
                operande2 := pop(F_calculator);
                push(F_calculator , operande1 * operande2);
            end if;
        end mul;

        -- nom : div
        -- sémantique : take the 2 first element of the stack and perform addition, then put the result in the stack
        -- paramètres : F_calculator: In out t_calculator 
        -- pré-condition : 
        -- post-condition : nb_elements -1
        -- exception : not_enougth_operande when nb_elements <2
        --             zero_devide_exception when operand2 = 0
        -- test :   
        procedure div (F_calculator : in out t_calculator) is
            operande1 : Integer;
            operande2 : Integer;
        begin
            if F_calculator.nb_elements < 2 then
                raise not_enougth_operande ;
            else
                operande1 := pop(F_calculator);
                operande2 := pop(F_calculator);
                if operande2 = 0 then
                    push(F_calculator , operande1);
                    push(F_calculator , operande2);
                    raise zero_devide_exception;
                else
                    push(F_calculator , operande1 / operande2);
                end if;
            end if;
        end div;
end p_calculatrice;