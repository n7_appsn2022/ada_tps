-- Callune Gitenet SN APP 1A
-- paquet qui implemente la calculatrice polonaise
with p_pile;
package p_calculatrice is
    -- instanciation de la gé&néricité 
    package p_pile_entier is new p_pile(T_generic => Integer);

    --use p_pile_entier;
    -- constante declaration
    -- exception declaration
    zero_devide_exception: exception;
    not_enougth_operande: exception;
    -- types declaration
    Type t_calculator is private;
    -- procedure & function block

        -- nom : init
        -- sémantique : initialize the calculator
        -- type de retour : t_stack
        -- paramètres : 
        -- pré-condition : 
        -- post-condition : isEmpty
        -- exception : 
        -- test :  

        function init return t_calculator;

        -- nom : reset
        -- sémantique : reset
        -- paramètres :  F_calculator : in out t_calculator
        -- pré-condition : 
        -- post-condition : 
        -- exception : 
        -- test :  

        procedure reset (F_calculator: in out t_calculator) ;

        -- nom : push
        -- sémantique : stack number on top 
        -- paramètres : F_calculator: In/Out t_calculator 
        --              F_nouveau : In Integer 
        -- pré-condition : 
        -- post-condition : F_nouveau apartien F_calculator
        -- exception : 
        -- test :   
        procedure push (F_calculator : in out t_calculator; F_nouveau : in Integer );

        -- nom : pop
        -- return Integer
        -- sémantique : pop number on top 
        -- paramètres : F_calculator: In/Out t_calculator 
        -- pré-condition : 
        -- post-condition : F_calculator.nbeleme = nbelem -1
        -- exception : 
        -- test :   
        function pop (F_calculator : in out t_calculator) return Integer;


        -- nom : add
        -- sémantique : take the 2 first element of the stack and perform addition, then put the result in the stack
        -- paramètres : F_calculator: In out t_calculator 
        -- pré-condition : 
        -- post-condition : nb_elements -1
        -- exception : not_enougth_operande when nb_elements <2
        -- test :   
        procedure add (F_calculator : in out t_calculator);

        -- nom : sub
        -- sémantique : take the 2 first element of the stack and perform substraction, then put the result in the stack
        -- paramètres : F_calculator: In out t_calculator 
        -- pré-condition : 
        -- post-condition : nb_elements -1
        -- exception : not_enougth_operande when nb_elements <2
        -- test :   
        procedure sub (F_calculator : in out t_calculator);

        -- nom : mul
        -- sémantique : take the 2 first element of the stack and perform multiplication, then put the result in the stack
        -- paramètres : F_calculator: In out t_calculator 
        -- pré-condition : 
        -- post-condition : nb_elements -1
        -- exception : not_enougth_operande when nb_elements <2
        -- test :   
        procedure mul (F_calculator : in out t_calculator);

        -- nom : div
        -- sémantique : take the 2 first element of the stack and perform addition, then put the result in the stack
        -- paramètres : F_calculator: In out t_calculator 
        -- pré-condition : 
        -- post-condition : nb_elements -1
        -- exception : not_enougth_operande when nb_elements <2
        --             zero_devide_exception when operand2 = 0
        -- test :   
        procedure div (F_calculator : in out t_calculator);

private 
    type t_calculator is record
            stack : p_pile_entier.t_stack;
            nb_elements : Integer ;
    end record;

end p_calculatrice;