-- Callune Gitenet SN APP 1A
-- gère un tableau de valeur avec une implémentation bitmap
-- with use block
with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io ;

with ada.float_text_io;
use ada.float_text_io ;

procedure bitmap is

-- constante declaration
NMAX : constant integer := 10 ; 
-- types declaration
type t_arr_float is array (1..NMAX) of float; 
type t_arr_boolean is array (1..NMAX) of boolean; 
type t_bitmap is record
    tab_value : t_arr_float;
    tab_flag : t_arr_boolean;
    imin : integer:=1;
    imax : integer:= 1;
    nb_elem : integer:= 0;
end record;

-- procedure & function block
    -- nom : add
    -- sémantique : ajoute l'élement dans la bitmap
    -- paramètres : F_bitmap: In/Out t_bitmap 
    --              F_elem: In float --element à ajouter
    -- pré-condition : nb_elem < NMAX
    -- post-condition : f_elem apartien à F_bitmap, nb_elem'aprés = nb_elem'avant +1 
    -- exception : 
    -- test :   
    procedure add (
        F_bitmap : in out t_bitmap;
        F_elem : in float ) is
        -- déclaration variables locales
        insert : boolean := false;
        i : integer ;
    begin
        -- R1.1 cas ou on insert entre imin et imax
        i := F_bitmap.imin;
        while not insert and then i <= F_bitmap.imax loop
            if F_bitmap.tab_flag(i) /= true then
                F_bitmap.tab_flag(i) := true;
                F_bitmap.tab_value(i) := F_elem;
                insert := true ;
                F_bitmap.nb_elem := F_bitmap.nb_elem + 1;
            end if;
            i := i + 1;
        end loop;

        -- R1.2 cas ou on insert en desous de imin
        if not insert and then F_bitmap.imin >1 then
            F_bitmap.imin := F_bitmap.imin -1;
            i:= F_bitmap.imin;
            F_bitmap.tab_flag(i) := true;
            F_bitmap.tab_value(i) := F_elem;
            insert := true ;
            F_bitmap.nb_elem := F_bitmap.nb_elem + 1;
        end if;

        -- R1.3 cas ou on insert au dessus de imax
        if not insert and then F_bitmap.imax < NMAX then
            F_bitmap.imax := F_bitmap.imax +1;
            i:= F_bitmap.imax;
            F_bitmap.tab_flag(i) := true;
            F_bitmap.tab_value(i) := F_elem;
            insert := true ;
            F_bitmap.nb_elem := F_bitmap.nb_elem + 1;
        end if;
    end add;

    -- nom : remove
    -- sémantique : suprime l'élement dans la bitmap à l'indice i
    -- paramètres : F_bitmap: In/Out t_bitmap 
    --              F_index: In integer --index de l'element à suprimé 
    -- pré-condition : 
    -- post-condition : l'élement est suprimé 
    -- exception : 
    -- test :   
    procedure remove (
        F_bitmap : in out t_bitmap;
        F_index : in integer ) is
        -- déclaration variables locales
    begin
        if F_index = F_bitmap.imin then
        F_bitmap.imin := F_bitmap.imin +1;
        elsif F_bitmap.imax = F_index then
        F_bitmap.imax := F_bitmap.imax -1;
        end if;
        F_bitmap.tab_flag(F_index) := false;
    end remove;

    -- nom : print
    -- sémantique : affiche la bitmap
    -- paramètres : F_bitmap: In t_bitmap 
    -- pré-condition : 
    -- post-condition : affiche la bitmap dans le terminal
    -- exception : 
    -- test :   
    procedure print (F_bitmap : in t_bitmap ) is
        -- déclaration variables locales
        value: STRING(1..5); 
        value_length: integer;
    begin
        -- R1 premier affichage
        put_line("Indice min ="&integer'image(F_bitmap.imin));
        put_line("Indice max ="&integer'image(F_bitmap.imax));
        for i in F_bitmap.imin..F_bitmap.imax loop 
            if F_bitmap.tab_flag(i) then 
                value(1..4) := "true";
                value_length := 4;
            else
                value(1..5) := "false";
                value_length := 5;
            end if;
            put(integer'image(i)&"   "&value(1..value_length)&"   ");
            put(F_bitmap.tab_value(i),1,1,0);
            new_line;
        end loop;

        -- R2 second affichage
        put_line("Nb effectif d'éléments ="&integer'image(F_bitmap.nb_elem));
        for i in F_bitmap.imin..F_bitmap.imax loop 
            if F_bitmap.tab_flag(i) then 
                put(F_bitmap.tab_value(i),1,1,0);
                new_line;
            end if;
        end loop;
    end print;

    -- nom : get_first
    -- sémantique :
    -- type de retour : <type>
    -- paramètres : F_<var_name>: In/Out <Type> --commentaire
    --              F_<var_name>: In <Type> --commentaire
    --              F_<var_name>: Out <Type> --commentaire
    -- pré-condition : 
    -- post-condition : F_X'Aprés = F_Y'Avant
    -- exception : 
    -- test :   entré => sortie

    function get_first (
        F_bitmap : in t_bitmap;
        F_elem : in float) return integer is
        --déclaration variables locales
    begin
        --parcours les valeurs effectives
        for i in F_bitmap.imin..F_bitmap.imax loop
            if F_bitmap.tab_value(i) = F_elem and F_bitmap.tab_flag(i) then
                return i;
            end if;
        end loop;
        return 0;
    end get_first;

    -- nom : compact
    -- sémantique : compacte la bitmap en enlevant les trous
    -- paramètres : F_bitmap: In/out t_bitmap 
    -- pré-condition : 
    -- post-condition : compacte la bitmap en enlevant les trous entre imax et imin
    -- exception : 
    -- test :   
    procedure compact (F_bitmap : in out t_bitmap ) is
        -- déclaration variables locales
        perm_index: integer;
        found:boolean;
    begin
        --R1.1 parcourir les case effective
        if F_bitmap.imax-F_bitmap.imin /= F_bitmap.nb_elem then
            for i in F_bitmap.imin..F_bitmap.nb_elem+F_bitmap.imin loop
                -- si c'est une valeur non significative
                if not F_bitmap.tab_flag(i) then
                    --R2 On permute avec la prochaine valeur vrai
                    perm_index := i;
                    found := false;
                    while not found and perm_index <F_bitmap.imax loop
                        if F_bitmap.tab_flag(perm_index) then
                            F_bitmap.tab_flag(i) := true;
                            F_bitmap.tab_flag(perm_index) := false;
                            found :=true;
                            F_bitmap.tab_value(i) :=  F_bitmap.tab_value(perm_index);
                        end if;
                        perm_index:= perm_index+1;
                    end loop;
                end if;
            end loop;
        end if;
    end compact;

-- main's variable
bitmap: t_bitmap ;

begin 
    --R1 init
    bitmap.tab_flag(1) := false ;
    add(bitmap,1.0);
    add(bitmap,2.0);
    add(bitmap,3.0);
    add(bitmap,4.0);
    add(bitmap,5.0);
    remove(bitmap,1);
    remove(bitmap,3);
    remove(bitmap,5);
    add(bitmap,2.5);
    add(bitmap,1.5);
    add(bitmap,4.0);
    compact(bitmap);
    put_line(integer'image(get_first(bitmap,4.0)));
    put_line(integer'image(get_first(bitmap,1.0)));
    remove(bitmap,3);
    remove(bitmap,4);
    put_line(integer'image(get_first(bitmap,4.0)));
    compact(bitmap);
    print(bitmap);

end bitmap;