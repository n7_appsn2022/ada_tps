-- Callune Gitenet SN APP 1A
-- permet la manipulation d'un octet (8 bit dans un tableau)

-- with use block
with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io ;

procedure binary is

-- constante declaration
MAX_BIT : constant integer := 7 ; 
-- types declaration
type t_byte is array (0..MAX_BIT) of integer range 0..1 ;
-- procedure & function block

    -- nom : afficher
    -- sémentique : affiche un nombre binaire
    -- paramètres : F_byte: In t_byte -- le binaire a afficher
    -- pré-condition : 
    -- post-condition : affiche t_byte dans la console
    -- exeption : 
    -- test :   [0,1,0,1,0,0,0,1] => "01010001"

    procedure afficher ( F_byte : in t_byte ) is
    -- déclaration variables locales
    begin
        for i in reverse 0..7 loop
            put(F_byte(i),1);
        end loop;
        New_Line;
    end afficher;

    -- nom : b10tob2
    -- sémentique : tranforme un nombre en base 10 en un nombre binaire
    -- type de retour : t_byte
    -- paramètres : F_b10: In integer  -- le nombre en base 10
    -- pré-condition : F_b10 < 256
    -- post-condition : F_b10 <=> t_byte
    -- exeption : 
    -- test :   7 => 1110 0000

    function b10tob2 (F_decimal : in integer) return t_byte is
    --déclaration variables locales
    i : integer := MAX_BIT; 
    reste : integer ;
    mon_byte :t_byte;
    begin
        --R1.1 On initialise notre byte
        for init in mon_byte'range loop
            mon_byte(init) := 0;
        end loop;
        --R1.2 on copie le param en in
        reste := F_decimal;

        --R1.3 on convertie
        while i>=0 and then reste /= 0 loop
            if 2**i <=reste then
                mon_byte(i):= 1;
                reste := reste - 2**i;
            end if;
            i := i-1 ;
        end loop;
        return mon_byte;
    end b10tob2;


    -- nom : b2tob10
    -- sémentique : tranforme un nombre en base 2 en un nombre décimale
    -- type de retour : integer
    -- paramètres : F_b2: In t_byte  -- le nombre en base 2
    -- pré-condition : 
    -- post-condition : F_b2 <=> l'entier retourner
    -- exeption : 
    -- test :    1110 0000 => 7

    function b2tob10 (F_byte : in t_byte) return integer is
    --déclaration variables locales
    sum : integer := 0 ;
    begin
        for i in F_byte'range loop
            if F_byte(i) = 1 then
                sum := sum + 2**i;
            end if;
        end loop;
        return sum;
    end b2tob10;


    -- nom : add
    -- sémentique : ajoute 2 binaire entre eux
    -- type de retour : t_byte
    -- paramètres : F_byte_a: In t_byte  -- le 1er nombre en base 2
    --              F_byte_b: In t_byte  -- le 2eme nombre en base 2
    -- pré-condition : F_byte_a + F_byte_b < 256
    -- post-condition :  retourne F_byte_a + F_byte_b
    -- exeption : 
    -- test :    0000 0111 + 0011 0000 => 0011 0111

    function add (F_byte_a, F_byte_b : in t_byte) return t_byte is
    --déclaration variables locales
    begin
        return b10tob2(b2tob10(F_byte_a)+b2tob10(F_byte_b));
    end add;
-- main's variable
mon_byte : t_byte ;

begin 
    for i in 0..10 loop
        put_line(integer'Image(i));
        mon_byte := b10tob2(i);
        afficher(mon_byte);
        put_line(integer'Image(b2tob10(mon_byte)));
        put_line(integer'Image(b2tob10(add(mon_byte,mon_byte))));
        New_Line;
    end loop;

end binary;