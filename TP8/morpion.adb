-- Callune Gitenet SN APP 1A
-- jeu du morpion
-- with use block
with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io ;

procedure morpion is
    -- constante declaration
    KMAX: constant integer := 10; -- taille max du damier
    -- types declaration
    type t_symbol is (LIBRE, ROND, CROIX);--contenu d'une case du damier
	type t_damier is array(1..kmax, 1..kmax) of t_symbol; -- un damier
	type t_joueur is (JROND, JCROIX); -- 2 joueur
	type t_etat_jeu is (EN_COURS, GAGNE, NUL); 
    -- procedure & function block
        -- nom : init															
        -- sémantique : Initialise un nouveau damier avec F_size x F_size cases
        -- paramètres : F_damier: OUT t_damier - Le damier à initialiser 		
        -- 			    F_size: OUT integer - La taille du damier (F_size x F_size) 
        -- 			    F_first_player: OUT t_joueur - Le joueur qui commence (Jrond/Jcroix)
        -- précondition :
        -- postcondition : Le damier F_damier est de taille F_size, le joueur F_first_player commence 
        -- exception : 
        -- test :  
        procedure init(F_damier: OUT t_damier; F_size: OUT integer; F_first_player: OUT t_joueur) is
            size: integer;
            str_player : character;
        begin
            -- R1 saisie contrôlé de la taille
            loop
                put_line("Jeu du morpion - Quelle taille ? (entre 3 et 10)");
                get(size);
                exit when size >= 3 and size <= 10;
            end loop;
            F_size := size;

            -- R1 saisie contrôlé du premier joueur
            loop
                put_line("Quel joueur commence ? (x / o)");
                get(str_player);
                exit when str_player = 'x' or str_player = 'o';
            end loop;
            F_first_player := (if str_player = 'x' then JCROIX else JROND);

            -- R1 on rend toute les case du damier libre
            for i in 1..size loop
                for j in 1..size loop
                    F_damier(i,j) := LIBRE;
                end loop;
            end loop;
        end init;

        -- nom : print
        -- sémantique : affiche l'etat du jeu en cours
        -- paramètres : F_damier: in t_damier 		
        -- 			    F_size: in integer 
        -- 			    F_player: in t_joueur 
        -- pré-condition : 
        -- post-condition : affiche le jeu en cours
        -- exception : 
        -- test :   
        procedure print (F_damier: in t_damier; F_size: in integer; F_player: in t_joueur)is
        -- déclaration variables locales
        begin
            -- R1.1 on affiche la grille de jeu
            for i in 1..F_size loop
                for j in 1..F_size loop
                    case F_damier(i,j) is
                        when ROND =>  put(" O ");
                        when CROIX => put(" X ");
                        when others   => put("   ");
                    end case;
                    if j /= F_size then
                            put("|");
                    end if;
                end loop;
                new_line;
                for k in 1..F_size loop
                    put("----");
                end loop;
                new_line;
            end loop;

            -- R1.2 on affiche le joueur courant
            new_line;
            put("Joueur courant: ");
            if F_player = JCROIX then
                put("X");
            else
                put("O");
            end if;
            new_line;
        end print;

        -- nom : print_end
        -- sémantique : affiche le bilan de la partie
        -- paramètres : F_damier: in t_damier 		
        -- 			    F_size: in integer 
        -- 			    F_player: in t_joueur 
        --              F_etat: in t_etat_jeu
        -- pré-condition : 
        -- post-condition : affiche le jeu en cours
        -- exception : 
        -- test :   
        procedure print_end (F_damier: in t_damier; F_size: in integer; F_player: in t_joueur; F_etat: in t_etat_jeu)is
        -- déclaration variables locales
        begin
            -- R1.1 on affiche la grille de jeu
            for i in 1..F_size loop
                for j in 1..F_size loop
                    case F_damier(i,j) is
                        when ROND =>  put(" O ");
                        when CROIX => put(" X ");
                        when others   => put("   ");
                    end case;
                    if j /= F_size then
                            put("|");
                    end if;
                end loop;
                new_line;
                for k in 1..F_size loop
                    put("----");
                end loop;
                new_line;
            end loop;
            -- R1.2 On affiche le bilan de partie
            if F_etat = GAGNE then
			    put("Le joueur ");
                if F_player = JCROIX then
                    put("X ");
                else
                    put("0 ");
                end if;
			    put("a gagné la partie.");
		    else
			    put("Egalité");
		    end if;
            new_line;
        end print_end;

        -- nom : isFull
        -- sémantique : vérifie qu'on peut encore jouer et qu'il n'y a pas égalité
        -- paramètres : F_damier: in out t_damier 		
        -- 			    F_size: in integer 
        -- return boolean true si le jeu est plein false si on peut encore jouer
        function isFull(F_damier: IN t_damier; F_size: IN integer) return boolean is
            row :integer :=1;
            line : integer :=1;
            isFullBool : boolean:= true;
            begin
                while isFullBool and then line <= F_size loop
                    while isFullBool and then row <= F_size loop
                        if F_damier(line, row) = LIBRE then
                            isFullBool:= false;
                        end if;
                        row:= row+1;
                    end loop;
                    line := line +1;
                end loop;
                return isFullBool;
            end isFull;
        
        -- nom : isWin
        -- sémantique : vérifie si le joueur viens de gagné
        -- paramètres : F_damier: in  t_damier 		
        -- 			    F_size: in integer 
        --              F_play_line : in integer
        --              F_play_row : in integer
        --              F_player: in t_joueur
        -- return boolean true si le joueur a gagné
        function isWin(F_damier: IN t_damier; F_size: IN integer;
         F_play_line: IN integer;F_play_row: IN integer;
         F_player:IN t_joueur) return boolean is
            pion : t_symbol;
            isWon : boolean :=false;
            diagoaligned : boolean:= false;
            x : integer;
            y : integer;
        begin
        	pion := (if F_player = JCROIX then CROIX else ROND);
            --R1.1 et R1.2 On check la ligne et la colone
            y := 0;
            x:= 0;
            for i in 1..F_size loop
                -- on check la ligne
                if F_damier(F_play_line,i)=pion then
                    x := x+1;
                end if;
                -- on check la colone
                if F_damier(i,F_play_row)=pion then
                    y := y+1;
                end if;
            end loop;
            if x = F_size or y = F_size then
                isWon := true;
            end if;
            --R1.3 On check diagonal /
            x := F_size;
            y := 1;
            while not isWon and  y <= F_size-1 loop
                if F_damier(x,y)=F_damier(x-1,y+1) and F_damier(x,y)=pion then
                    diagoaligned := true;
                else
                    diagoaligned := false;
                end if;
                x:= x-1;
                y:= y+1;
            end loop;
            if diagoaligned then 
                isWon := true;
            end if;
            --R1.4 On check diagonal \
            y := 1;
            x := 1;
            while not isWon and  x <= F_size-1 loop                
            --vérif précédent = suivant
                if F_damier(x,y)=F_damier(x+1,y+1) and F_damier(x,y)=pion then
                    diagoaligned := true;
                else
                    diagoaligned := false;
                end if;
                y := y+1;
                x := x+1;
            end loop;
            if diagoaligned then 
                isWon := true;
            end if;
            return isWon;
        end isWin;


        -- nom : play
        -- sémantique : joue le tour 
        -- paramètres : F_damier: in out t_damier 		
        -- 			    F_size: in integer 
        -- 			    F_player: in out t_joueur 
        --              F_etat: in out t_etat_jeu
        -- pré-condition : 
        -- post-condition : affiche le jeu en cours
        -- exception : 
        -- test :   
        procedure play (F_damier: in out t_damier; F_size: in integer; F_player: in out t_joueur; F_etat: in out t_etat_jeu)is
        -- déclaration variables locales
        	play_row: integer;
            play_line: integer;
        begin
            -- R1 on fait saisir une case libre
            loop
                loop
                    put_line("Sur quelle colone placer le pion ?");
                    get(play_row);
                exit when play_row > 0 and play_row <= F_size;
                end loop;

                loop
                    put_line("Sur quelle ligne placer le pion ?");
                    get(play_line);
                exit when play_line > 0 and play_line <= F_size;
                end loop;

                if F_damier(play_line,play_row) /= LIBRE then
                    put_line("La case est prise.");
                end if;
            exit when F_damier(play_line,play_row) = LIBRE;	
            end loop;
            --R1.2 On remplit la case avec le pion du joueur
            F_damier( play_line,play_row) := (if F_player = JROND then ROND else CROIX);

            --R1.3 on vérifie l'etat du jeu aprés ce coup
            if isWin(F_damier, F_size, play_line,play_row, F_player) then
                put_line("iswin true");
                F_etat := GAGNE;
            elsif isFull(F_damier, F_size) then
                put_line("isfull true");
                    F_etat := NUL;
            else
                put_line("on continue");
                F_etat := EN_COURS;
                F_player := (if F_player = JROND then JCROIX else JROND);
            end if;
            put_line("end play");

        end play;
    -- main's variable
    damier: t_damier;
	n: integer; -- dimension effective du damier entre 3 et kmax
	joueur: t_joueur; -- le joueur courant
	etat: t_etat_jeu := EN_COURS; -- l'etat courant
begin 
    --R1 initialiser le jeu
    init(damier,n,joueur);

    --R1 Jouer
    loop
        -- afficher le damier et le joueur courant
        print(damier,n,joueur);
        -- faire jouer le joueur courant
        play(damier,n,joueur,etat);
    exit when etat /= EN_COURS;
    end loop;

    -- afficher la fin de partie
    print_end(damier,n,joueur,etat);
end morpion;