-- Callune Gitenet SN APP 1A
-- affiche le n eme terme de la suite de fibonacci
-- précondition: n>1
-- postcondition : affiche le terme n
-- test : 

-- with use block
with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io;


procedure fibonacci_ada is

-- constante declaration
-- types declaration
-- procedure & function block
-- main's variable
fib0 : Integer ; -- terme 0 de a suite
fib1 : Integer ; -- terme 1 de la suite 
n :Integer ; -- entier lu au clavier
fibn : Integer ; -- calcule de fib(n)
resultat : Integer ; 

begin 

    -- R0 fibonacci
    -- R1.0 lire n
    loop
        Put_line("Saisir n >= 2 :");
        Get(n);
    exit when n>=2;
    end loop;
    -- R1.1 initialisation des 2 premier termes
    fib0 := 0;
    fib1 :=1 ;
    -- R1.2 calcule du terme suivant
    for i in 2..n loop
        fibn := fib1 + fib0;
        fib0 := fib1;
        fib1 := fibn;
    end loop;
    -- R1.3 affichage
    resultat := fibn;
    Put_line("le n-iéme terme de la suite est  "& Integer'Image(resultat) &".");


end fibonacci_ada;