-- Callune Gitenet SN APP 1A
-- affiche la puissance entiere d'un réel
-- précondition : expo != 0
-- postcondition : resultat = val^expo
-- test : val =2 ewpo =3 => 8
-- val = 2 expo = -3 => 0.125
-- val = 2 expo =1 => 2


-- with use block
with ada.Float_Text_IO, ada.Text_IO;
use ada.Float_Text_IO, ada.Text_IO;
with ada.integer_text_io;
use ada.integer_text_io;


procedure puissance_ada is

-- constante declaration
-- types declaration
-- procedure & function block
-- main's variable
expo: Integer ; --la puissance entier à appliquer
val: Float ; -- réel a mettre a la puissance expo
resultat: Float; -- résultat
tmp_resultat : Float ; --variable de calcule de resultat
begin 

    -- R0 calcule d'une puissance
    -- R1.1 saisir val
    Put_line("Saisir la valeur réel");
    Get(val);
    -- R1.2 saisir expo
    Put_line("Saisir l'exposant");
    Get(expo);
    -- R1.3 multiplier val par val expo fois 
    tmp_resultat :=val ;
    for i in 2..abs(expo) loop
            tmp_resultat:= tmp_resultat * val;
    end loop;
    -- R2.1  de R1.3 traitement du cas négatif
    if expo >0 then 
        resultat := tmp_resultat;
    else 
        resultat := 1.0/tmp_resultat;
    end if ;
    
    -- R1.4 afficher resultat
Put_Line(Float'Image(val)& " puissance  "& Integer'Image(expo)&" donne "& Float'Image(resultat));


end puissance_ada;