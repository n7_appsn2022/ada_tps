#!/usr/bin/python3
''' 
    Callune Gitenet SN APP 1A
 afficher la somme des entiers entre m & n  saisie 
 précondition n>= m et n&m >=0 (si par contrat )
 test : n =2 ,m = 1 => 3
 n=4 ,m=2  => 9 
 n=1 m=1 => 1
'''

# R0 afficher la somme des entiers naturel entre m et n 
#R1.1 saisir m >= 0  out m
m = int(input("Saisir m >= 0 : "))
while m<0:
    m = int(input("Saisir m >= 0 : "))

#R1.2 saisir n >= m  out n
n = int(input("Saisir n >= m : "))
while n<0:
    n = int(input("Saisir n >= m : "))
#R1.3 calculer la somme des entiers de m à n  in/out somme in m,n
somme = 0
for i in range (m,n+1):
    somme = somme + i
#R1.4 afficher la somme   in somme
print("La somme des entiers de ",m," à ",n, " vaut ",somme)            
