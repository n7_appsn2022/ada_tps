-- Callune Gitenet SN APP 1A
-- afficher la somme des entiers entre m & n  saisie 
-- précondition n>= m et n&m >=0 (si par contrat )
-- test : n =2 ,m = 1 => 3
-- n=4 ,m=2  => 9 
-- n=1 m=1 => 1

-- with use block
with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io;

procedure somme_ada is

-- constante declaration
-- types declaration
-- procedure & function block
-- main's variable
 n : Integer ; --début de l'intervalle
 m : Integer ; -- fin de l'intervalle
 somme : Integer ; -- la somme des entier  

begin 

-- R0 afficher la somme des entiers naturel entre m et n 
    --R1.1 saisir m >= 0  out m
    loop
        Put_line("Saisir m >= 0 :");
        Get(m);
    exit when m>=0;
    end loop;
    --R1.2 saisir n >= m  out n
    loop
        Put_line("Saisir n >= m");
        Get(n);
    exit when n>=m;
    end loop;
    --R1.3 calculer la somme des entiers de m à n  in/out somme in m,n
    somme:=0;
    for i in m..n loop
        somme:= somme+i;
    end loop;
    --R1.4 afficher la somme   in somme
    Put_line("la somme des entier de "& Integer'Image(m) &" a "& Integer'Image(n) &" est égale a "& Integer'Image(somme) & ".");
end somme_ada;