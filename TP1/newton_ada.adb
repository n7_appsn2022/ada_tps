-- Callune Gitenet SN APP 1A
-- calcule la racine d'un nombre x a la précision epsilon
-- précondition x>0
-- postcondition : racine = racine de x a la précision epsilon
-- test : x =4 => 2

-- with use block
with ada.Float_Text_IO, ada.Text_IO;
use ada.Float_Text_IO, ada.Text_IO;

procedure newton_ada is

-- constante declaration
-- types declaration
-- procedure & function block
-- main's variable
 x : Float ; -- réel dont on veux la racine
 ak : Float ; -- aproximation de la racine
 ak1 : Float ; -- aproximation de la racine
 epsilon : Float ; -- précision
 racine : Float ; --raçine de x a la précision epsilon retourné 

begin 

-- R0 calcule de la racine carrée d'un nombre 
-- R1.1 Saisir x réel in x --out x
    loop
        Put_line("Saisir x >= 0 :");
        Get(x);
    exit when x >= 0.0;
    end loop;
-- R1.2 saisir e réel précission in epsilon  --out epsilon
    loop
        Put_line("Saisir epsilon > 0 :");
        Get(epsilon);
    exit when epsilon>0.0;
    end loop;

    
-- R1.3 calcule de la racine carrée in x,epsilon,ak,ak1 out racine
   ak := 1.0;
   ak1 := 0.5 * (ak + x/ak);
    --R2.1 formule de newton in ak1,ak,x
   while abs(ak1-ak) >= epsilon loop
      ak := ak1;
      ak1 := 0.5 * (ak + x/ak);
   end loop;
    racine := ak1 ;
-- R1.4 afficher la racine in racine 
Put_Line("La racine de " & Float'Image(x)& " est : "& Float'Image(racine));
end newton_ada;