-- Callune Gitenet SN APP 1A
-- sasie une string et remplace les a en b
-- postcondition : toute les ocurences de a devienent b
-- test : zbradaraljan => zbrbdbrbjbn

-- with use block
with Ada.Text_IO; use Ada.Text_IO;

procedure a_to_b is

-- constante declaration
CMAX : constant INTEGER := 80; 
-- types declaration
-- procedure & function block
-- main's variable
str : STRING(1..CMAX);  -- chaine saisie au clavier
str_length : INTEGER; 
begin 
    -- R1 saisir une chaine et sa taille 
    put_line("saisie ta chaine");
    get_line(str, str_length);
    put_line("vous avez saisie : " & str(1..str_length));

    -- R1 transformer les a en b
    for i in 1..str_length loop
        if str(i) = 'A' then
            str(i) := 'B';
        end if;
        if str(i) = 'a' then
            str(i) := 'b';
        end if;
    end loop;

    -- R1 afficher le resultat
    put_line("après transformation on a : " & str(1..str_length));

end a_to_b;