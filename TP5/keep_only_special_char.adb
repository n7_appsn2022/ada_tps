-- Callune Gitenet SN APP 1A
-- sasie une string et en supprime les lettre
-- postcondition : toute les lettres disparaisse
-- test : il fait beau. => .

-- with use block
with Ada.Text_IO; use Ada.Text_IO;

procedure keep_only_special_char is

-- constante declaration
CMAX : constant INTEGER := 80; 
-- types declaration
-- procedure & function block
-- main's variable
str : STRING(1..CMAX);  -- chaine saisie au clavier
str_length : INTEGER; 
modified_str : STRING(1..CMAX);
modified_str_length : INTEGER := 0;

begin 
    -- R1 saisir une chaine et sa taille 
    put_line("saisie ta chaine");
    get_line(str, str_length);
    put_line("vous avez saisie : " & str(1..str_length));

    -- R1 supprimer les lettre
    for i in 1..str_length loop
        if (CHARACTER'POS(str(i)) >= CHARACTER'POS('A') and CHARACTER'POS(str(i)) <= CHARACTER'POS('Z') ) or ( CHARACTER'POS(str(i)) >= CHARACTER'POS('a') and CHARACTER'POS(str(i)) <= CHARACTER'POS('z') ) then
            null; -- on ne copie pas le caractère
        else
            modified_str(modified_str_length +1) := str(i);
            modified_str_length := modified_str_length +1;
        end if;
    end loop;

    -- R1 afficher le resultat
    put_line("après transformation on a : " & modified_str(1..modified_str_length));

end keep_only_special_char;