-- Callune Gitenet SN APP 1A
-- sasie une string n'en garde que les lettre
-- postcondition : seule les lettres restent
-- test : il / fait " beau. => il fait beau

-- with use block
with Ada.Text_IO; use Ada.Text_IO;

procedure keep_only_letter is

-- constante declaration
CMAX : constant INTEGER := 80; 
-- types declaration
-- procedure & function block
-- main's variable
str : STRING(1..CMAX);  -- chaine saisie au clavier
str_length : INTEGER; 
current_char : INTEGER := 1; -- iterateur de string


begin 
    -- R1 saisir une chaine et sa taille 
    put_line("saisie ta chaine");
    get_line(str, str_length);
    put_line("vous avez saisie : " & str(1..str_length));

    -- R1 supprimer les lettre
    while current_char <= str_length loop
        if (CHARACTER'POS(str(current_char)) >= CHARACTER'POS('A') and CHARACTER'POS(str(current_char)) <= CHARACTER'POS('Z') ) or ( CHARACTER'POS(str(current_char)) >= CHARACTER'POS('a') and CHARACTER'POS(str(current_char)) <= CHARACTER'POS('z') ) then
            null; -- c'est un caracter, on le garde
            current_char := current_char+1 ;
        else
            -- R2 On décale tout les suivants vers la guauche (ce qui écrase le courant)
            -- R3 on gère le cas ou on doit suprimer le dernier
            if current_char /= str_length then
                for to_shift in current_char..str_length-1 loop
                    str(to_shift):= str(to_shift+1);
                end loop;
                str_length := str_length -1 ;
            end if;
        end if;
    end loop;

    -- R1 afficher le resultat
    put_line("après transformation on a : " & str(1..str_length));

end keep_only_letter;