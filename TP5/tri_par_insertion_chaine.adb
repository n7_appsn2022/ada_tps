-- Callune Gitenet SN APP 1A
-- R0 : Trier par insertion d'une string puis afficher la string trié
-- Post-condition : La string est trié et affiché

-- Pour E/S sur les chaines de caractère
with text_io;
use text_io;

procedure tri_par_insertion_chaine is

-- constante declaration
CMAX : constant INTEGER := 80; 
-- types declaration
-- procedure & function block
-- main's variable
    str : STRING(1..CMAX);  -- chaine saisie au clavier
    str_length : INTEGER; 
    char : CHARACTER;       -- Pour mémorisation du character contenu à un certain indice
    j : Integer;        -- Variable utilisée pour le décalage d'éléments supérieurs à char

begin
     -- R1.1 : Saisir une chaine et sa taille et l'afficher
    put_line("saisie ta chaine");
    get_line(str, str_length);
    put_line("vous avez saisie : " & str(1..str_length));

    -- R1_2 : Parcours de la totalité de la tring pour la trier
    for i in 2..str_length loop
        -- R2_1 : Mémorisation du char contenu à l'indice i de la string, soit str(i)
        char := str(i);
        -- R2_2 : Décalage vers la droite des éléments supérieurs à ce même  char
        j := i;
        while j > 1 and then CHARACTER'POS(str(j-1)) > CHARACTER'POS(char) loop
            str(j) := str(j-1);
            j := j-1;
        end loop;
        -- R2_3 : Mettre le nombre qui était autrefois à l'indice i à la place qu'il faut
        str(j) := char;
    end loop;

    -- R1_3 : Afficher la nouvelle string après le tri
    put_line("après transformation on a : " & str(1..str_length));


end tri_par_insertion_chaine;