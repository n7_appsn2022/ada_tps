-- Callune Gitenet SN APP 1A
-- donne le nombre d'occurence de chaque lettre dans une string
-- postcondition : on a le nb d'ocurence de chaque lettre
-- test : aab => a = 2 b = 1 reste =0
with text_io; use text_io;
with ada.integer_text_io; use ada.integer_text_io;

procedure nb_occurence is
  CMAX : constant INTEGER := 80;
  type TAB_INT IS ARRAY(1..26) of INTEGER;
  tab : TAB_INT;
  chaine : STRING(1..CMAX);
  nbchar : INTEGER;
begin
  Put_Line("Veuillez rentrer une chaine de caractere :");
  Get_Line(chaine, nbchar);
  for i in 1..26 loop
    tab(i) := 0;
  end loop;
  for i in 1..nbchar loop
    if CHARACTER'POS(chaine(i)) >= CHARACTER'POS('a') and CHARACTER'POS(chaine(i)) <= CHARACTER'POS('z') then
      tab(1 +CHARACTER'POS(chaine(i)) - CHARACTER'POS('a')) := tab(1 +CHARACTER'POS(chaine(i)) - CHARACTER'POS('a')) + 1;
    end if;
  end loop;
  Put_Line("a b c d e f g h i j k l m n o p q r s t u v w x y z");
  for i in 1..26 loop
    Put(tab(i),1);
    Put(" ");
  end loop;
end nb_occurence;
