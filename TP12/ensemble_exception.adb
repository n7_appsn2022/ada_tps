-- Callune Gitenet SN APP 1A
-- Permet de manipuler des éléments dans un ensemble en gardant mesure du nombre d'éléments
-- with use block
with ada.integer_text_io;
use ada.integer_text_io;

with text_io;
use text_io;

with ada.float_text_io;
use ada.float_text_io;

procedure ensemble is
    -- constant declaration
    -- exception declaration
    ensemble_plein : exception;
    element_present : exception;
    ensemble_vide : exception;
    element_absent : exception;
    --taille max d'un ensemble
    NMAX : constant integer := 100;
    -- types declaration
    type elements is array(1..NMAX) of integer;
    type ensemble is record
        content : elements;
        cardinal : integer;
    end record;

    -- procedure & function block

        --nom : creer
        --sémantique : creer un ensemble vide
        --type de retour : ensemble
        --paramètres : aucun
        --pré-condition :
        --post-condition : crée un ensemble dont le cardinal est nul
        --exception :
        --test :
        function creer return ensemble is
            --déclaration variables locales
            retensemble : ensemble;
        begin
            retensemble.cardinal := 0;
            return retensemble;
        end creer;


        --nom : appartient
        --sémantique : vérifie la présence d'un élément dans un ensemble
        --type de retour : boolean
        --paramètres : F_ensemble: In ensemble --l'ensemble dont on check l'element
        --             F_element: In integer --l'element dont on vérifie la présence
        --pré-condition :
        --post-condition : vrai si e appartient a E ; Faux si e n'appartient pas a E
        function appartient (
            F_ensemble : in ensemble;
            F_element : in integer) return boolean is
            --déclaration variables locales
        begin
            for i in 1..F_ensemble.cardinal loop
                if F_ensemble.content(i) = F_element then
                    return true;
                end if;
            end loop;
            return false;
        end appartient;

        --nom : ajouter
        --sémantique : ajoute un element dans l'ensemble
        --paramètres : F_ensemble : In/Out ensemble --commentaire
        --             F_element : In integer
        --pré-condition : 
        --post-condition : update ensemble et cardinal
        --exception : ensemble plein => cardinal >= nmax
        --            element_present => element appartien ensemble
        --test :   entré => sortie

        procedure ajouter (
            F_ensemble : in out ensemble;
            F_element : in integer) is
            --déclaration variables locales
        begin
            --vérifie que le cardinal < nmax et que l'element n'y est pas déja
            if( F_ensemble.cardinal >= NMAX) then
                raise ensemble_plein;
            elsif appartient(F_ensemble,F_element) then
               raise element_present;
            else 
                F_ensemble.content(F_ensemble.cardinal+1):= F_element;
                F_ensemble.cardinal:= F_ensemble.cardinal +1 ;
            end if;
        end ajouter;

        --nom : supprimer
        --sémantique : supprime un element de l'ensemble
        --paramètres : F_ensemble: In/Out ensemble --commentaire
        --             F_element: In integer
        --pré-condition : element présent dans l'ensemble
        --post-condition : element enlevé de l'emsemble et cardinal maj
        --exception : ensemble_vide => l'ensemble est vide
        --            element_absent 
        --test :   entré => sortie

        procedure supprimer (
            F_ensemble : in out ensemble;
            F_element : in integer) is
            --déclaration variables locales
            cpt : integer;
            appartientBool : boolean := false;
        begin
            if F_ensemble.cardinal = 0 then
                raise ensemble_vide;
            elsif not appartient(F_ensemble,F_element) then
                raise element_absent;
            else
                cpt := 1;
                while cpt <= F_ensemble.cardinal and not appartientBool loop
                    cpt:=cpt+1;
                    if F_ensemble.content(cpt)=F_element then
                    appartientBool := true;
                    F_ensemble.cardinal := F_ensemble.cardinal -1;
                    end if;
                end loop;
            end if;
        end supprimer;

        --nom : afficher
        --sémantique : affiche le contenu de l'ensemble
        --paramètres : F_ensemble: In ensemble
        --pré-condition :
        --post-condition :
        --exception :
        --test :   entré => sortie

        procedure afficher (
            F_ensemble : in ensemble) is
            --déclaration variables locales
        begin
            put_line("afficher le contenu de l'ensemble");
            for i in 1..F_ensemble.cardinal loop
                put(F_ensemble.content(i));
            end loop;
            New_Line(1);
        end afficher;


    -- main's variable
    vensemble : ensemble;

begin
    vensemble := creer;

    ajouter(vensemble, 5);
    afficher(vensemble); -- 5 / 1

    ajouter(vensemble,4);
    afficher(vensemble); -- 5 4 / 2

    supprimer(vensemble, 4); -- 5 / 1
exception
    when ensemble_plein => put_line("l'ensemble est plein");
    when element_present => put_line("l'element est déja présent dans l'ensemble");
    when ensemble_vide => put_line("l'ensemble est vide !");
    when element_absent => put_line("l'element n'appartien pas a l'ensemble");
end ensemble;