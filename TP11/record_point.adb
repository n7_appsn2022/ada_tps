-- Callune Gitenet SN APP 1A
-- manipule les enregistrement avec pointeurs
-- with use block
    with text_io;
    use text_io;

    with ada.float_text_io;
    use ada.float_text_io ;

procedure record_point is
    -- constante declaration
    CODE_LENGTH : constant integer := 5;
    -- types declaration
    type tp_note is access float;
    type MATIERE is record 
        code : string(1..CODE_LENGTH);
        note : tp_note ;
    end record;
    -- procedure & function block
        -- nom : init
        -- sémantique : initialise le record
        -- paramètres : F_matiere: In/Out MATIERE 
        --              F_code : In string(1..CODE_LENGTH) 
        -- pré-condition : 
        -- post-condition :
        -- exception : 
        -- test :  

        procedure init (
            F_matiere: in out MATIERE;
            F_code : in string) is
        -- déclaration variables locales
        begin
            F_matiere.code := F_code;
            F_matiere.note := new float;
        end init;

        -- nom : print
        -- sémantique :
        -- paramètres : F_matiere: In MATIERE 
        -- pré-condition : 
        -- post-condition :
        -- exception : CONSTRAINT_ERROR lorsque F_matiere.note is null
        -- test :  

        procedure print (F_matiere: in  MATIERE) is
        -- déclaration variables locales
        begin
            put("Matiere code "&F_matiere.code&" note de ");
            put(F_matiere.note.all,1,2,0);
            new_line;
        end print;

        -- nom : set_note
        -- sémantique :
        -- paramètres : F_matiere: In out MATIERE 
        --              F_note_to_set : In Float
        -- pré-condition : 
        -- post-condition :
        -- exception : CONSTRAINT_ERROR lorsque F_matiere.note is null
        -- test :  

        procedure set_note (F_matiere: in out  MATIERE; F_note_to_set: in float) is
        -- déclaration variables locales
        begin
            F_matiere.note.all := F_note_to_set;
        end set_note;
    -- main's variable
    math : MATIERE;
begin 
   init(math,"maths");
   print(math);
   set_note(math,14.5);
   print(math);
exception
    when CONSTRAINT_ERROR => put_line("Cette Matiere n'as pas de note !");
end record_point;