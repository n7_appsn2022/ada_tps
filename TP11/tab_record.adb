-- Callune Gitenet SN APP 1A
-- manipule les enregistrement avec pointeurs
-- with use block
    with text_io;
    use text_io;

    with ada.float_text_io;
    use ada.float_text_io ;
    with ada.integer_text_io;
    use ada.integer_text_io ;

procedure tab_record is
    -- constante declaration
    CODE_LENGTH : constant integer := 5;
    MAX_NOTES : constant integer := 30;
    -- types declaration
    type tp_note is access float;
    type tp_notes is array (1..MAX_NOTES) of tp_note;
    type MATIERE is record 
        code : string(1..CODE_LENGTH);
        note : tp_notes ;
        nb_note : integer;
    end record;
    -- procedure & function block
        -- nom : init
        -- sémantique : initialise le record
        -- paramètres : F_matiere: In/Out MATIERE 
        --              F_code : In string(1..CODE_LENGTH) 
        --              F_nb_note :in integer
        -- pré-condition : 
        -- post-condition :
        -- exception : 
        -- test :  

        procedure init (
            F_matiere: in out MATIERE;
            F_code : in string;
            F_nb_note: in integer) is
        -- déclaration variables locales
        begin
            F_matiere.code := F_code;
            F_matiere.nb_note := F_nb_note;
            for i in 1..F_nb_note loop
                F_matiere.note(i):= new float;
            end loop;
        end init;

        -- nom : print
        -- sémantique :
        -- paramètres : F_matiere: In MATIERE 
        -- pré-condition : 
        -- post-condition :
        -- exception : CONSTRAINT_ERROR lorsque F_matiere.note is null
        -- test :  

        procedure print (F_matiere: in  MATIERE) is
        -- déclaration variables locales
        begin
            put("Matiere code "&F_matiere.code&" notes:");
            new_line;
            for i in 1..F_matiere.nb_note-1 loop
                put(F_matiere.note(i).all,1,2,0);
                new_line;
            end loop;
            new_line;
        end print;

        -- nom : set_note
        -- sémantique :
        -- paramètres : F_matiere: In out MATIERE 
        --              F_note_to_set : In Float
        -- pré-condition : 
        -- post-condition :
        -- exception : CONSTRAINT_ERROR lorsque F_matiere.note is null
        -- test :  

        procedure set_note (F_matiere: in out  MATIERE; F_index: in integer;F_note_to_set: in float) is
        -- déclaration variables locales
        begin
            F_matiere.note(F_index).all := F_note_to_set;
        end set_note;
    -- main's variable
    math : MATIERE;
begin 
   init(math,"maths",5);
   print(math);
   set_note(math,1,14.5);
   print(math);

end tab_record;