-- Callune Gitenet SN APP 1A
-- manipule les base des pointeurs
-- with use block
    with text_io;
    use text_io;

    with ada.integer_text_io;
    use ada.integer_text_io ;

procedure simple is
    -- constante declaration
    -- types declaration
    type PINT is access integer;
    -- procedure & function block
    -- main's variable
    p:PINT ;
begin 
    p:= new integer'(99);
    put_line(Integer'image(p.all));
    put_line("saisi un entier");
    get(p.all);
    put_line(Integer'image(p.all));

end simple;