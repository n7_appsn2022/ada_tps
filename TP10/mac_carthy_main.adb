-- Callune Gitenet SN APP 1A
-- fonction de MAC carthy
-- with use block
with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io ;

procedure mac_carthy_main is
    -- constante declaration
    -- types declaration
    -- procedure & function block
        -- nom : mac_carthy
        -- sémantique : implémentation de la fonction de mac_carthy
        -- type de retour : integer
        -- paramètres : F_n : In integer 
        -- pré-condition :  F_n >0
        -- post-condition : 
        -- exception : 
        -- test :   

        function mac_carthy (F_n : in integer) return integer is
        --déclaration variables locales
        begin
            if F_n >100 then 
                return F_n-10;
            else
                return mac_carthy(mac_carthy(F_n+11));
            end if;
        end mac_carthy;
    -- main's variable
    
begin 
    
    for i in 0..110 loop
        put_line(integer'image(i)&" : "& Integer'image(mac_carthy(i)));
    end loop;
end mac_carthy_main;