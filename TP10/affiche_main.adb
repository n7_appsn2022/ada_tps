-- Callune Gitenet SN APP 1A
-- affiche de manière recursive
-- with use block
    with text_io;
    use text_io;
    with ada.integer_text_io;
    use ada.integer_text_io ;

procedure affiche_main is

    -- constante declaration
    -- types declaration
    -- procedure & function block
         -- nom :affiche_ligne
        -- sémantique : affiche une ligne de manière recursive
        -- paramètres : F_m: In integer 
        --              F_n: In integer 
        -- pré-condition : 
        -- post-condition : 
        -- exception : 
        -- test :   

        procedure affiche_ligne (
            F_m : in integer;
            F_n : in integer)is
        -- déclaration variables locales
        begin
            if F_n >=0 then
                affiche_ligne(F_m,F_n-1); 
                put(10*F_m+F_n);
            end if;            
            
        end affiche_ligne;
       
        -- nom :affiche
        -- sémantique : affi he de manière recursive
        -- paramètres : F_m: In integer 
        --              F_n: In integer 
        -- pré-condition : 
        -- post-condition : 
        -- exception : 
        -- test :   

        procedure affiche (
            F_m : in integer;
            F_n : in integer)is
        -- déclaration variables locales
        begin
            if F_m >= 0  then
                affiche(F_m-1,F_n);                
                affiche_ligne(F_m,F_n);
                new_line;
            end if;            
        end affiche;


    -- main's variable
    
begin 
    for a in 0..10 loop
        for b in 0..10 loop 
            put(10*a+b);
        end loop;
        new_line;
    end loop;
    new_line;
    affiche(10,10);
end affiche_main;