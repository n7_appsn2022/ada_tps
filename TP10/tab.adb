-- Callune Gitenet SN APP 1A
-- manipule des tableau en recursif
-- with use block
with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io ;

procedure tab is

    -- constante declaration
    NMAX:constant integer := 100;
    -- types declaration
    type t_tab_integer is array(1..NMAX) of integer;
    -- procedure & function block
        -- nom : print_from_left
        -- sémantique : affiche le tab de la droite vers la gauche
        -- paramètres : F_tab: In t_tab_integer 
        --              F_nb_elem: In integer
        -- pré-condition : 
        -- post-condition : affiche le tab
        -- exception : 
        -- test :  

        procedure print_from_right (
            F_tab : in t_tab_integer;
            F_nb_elem : in integer ) is
        -- déclaration variables locales
        begin
            if F_nb_elem /= 0 then
                put_line(Integer'image(F_tab(F_nb_elem)));
                print_from_right(F_tab,F_nb_elem-1);
            end if;
        end print_from_right;

        -- nom : print_from_left
        -- sémantique : affiche le tab de la gauche vers la droite
        -- paramètres : F_tab: In t_tab_integer 
        --              F_nb_elem: In integer
        -- pré-condition : 
        -- post-condition : affiche le tab
        -- exception : 
        -- test :  

        procedure print_from_left (
            F_tab : in t_tab_integer;
            F_nb_elem : in integer ) is
        -- déclaration variables locales
        begin
            if F_nb_elem /= 0 then
                print_from_left(F_tab,F_nb_elem-1);
                put_line(Integer'image(F_tab(F_nb_elem)));
            end if;
        end print_from_left;

        -- nom : indexOf
        -- sémantique : renvoi l'index de la premiere ocurence de f_val dans f_tab
        -- type de retour : integer
        -- paramètres : F_tab: In t_tab_integer 
        --              F_nb_elem: In integer 
        --              F_val: In integer 
        -- pré-condition : 
        -- post-condition :  index ou 0 si non trouvé
        -- exception : 
        -- test :   
        function indexOf (
            F_tab : in t_tab_integer;
            F_nb_elem : in integer;
            F_val: in integer) return integer is
        --déclaration variables locales
        begin
            if F_nb_elem = 0 then
                return 0;
            elsif F_tab(F_nb_elem) = F_val then
                return F_nb_elem;
            else
                return indexOf(F_tab,F_nb_elem-1,F_val);
            end if;
        end indexOf;

    -- main's variable
    tab: t_tab_integer;
begin 
    for i in tab'range loop
        tab(i):=i;
    end loop;
    print_from_right(tab,NMAX);
    print_from_left(tab,NMAX);
    put_line(Integer'image(indexOf(tab,NMAX,50)));
end tab;