-- Callune Gitenet SN APP 1A
-- implementation d'une pile grace a une liste chainée d'T_generic
-- with use block

with text_io;
use text_io;

package body p_pile is
    -- procedure & function block

        -- nom : create_empty_stack
        -- sémantique : create_empty_stack
        -- type de retour : t_stack
        -- paramètres : 
        -- pré-condition : 
        -- post-condition : isEmpty
        -- exception : 
        -- test :  

        function create_empty_stack return t_stack is
            --déclaration variables locales
            new_stack :t_stack;
            begin
                new_stack := null;
                return new_stack;
        end create_empty_stack;

        -- nom : is_empty
        -- sémantique : is_empty
        -- type de retour : boolean
        -- paramètres :  F_stack : in t_stack
        -- pré-condition : 
        -- post-condition : vrai si is_empty
        -- exception : 
        -- test :  

        function is_empty (F_stack: in t_stack) return boolean is
            --déclaration variables locales
            begin
                return F_stack = null;
        end is_empty;

        -- nom : push
        -- sémantique : stack on top 
        -- paramètres : F_stack: In/Out t_stack 
        --              F_nouveau : In T_generic 
        -- pré-condition : 
        -- post-condition : F_nouveau apartien F_stack
        -- exception : 
        -- test :   
        procedure push (F_stack : in out t_stack;F_nouveau : in T_generic ) is
            -- déclaration variables locales
            newstack : t_stack;
            begin
                if F_stack = null then
                    F_stack := new t_cell_generic;
                    F_stack.all.element := F_nouveau;
                    F_stack.all.suivant := null;
                else
                    newstack := new t_cell_generic;
                    newstack.all.element := F_nouveau;
                    newstack.all.suivant := F_stack;
                    F_stack := newstack;
                end if;
        end push;

        -- nom : print
        -- sémantique : print the stack
        -- paramètres : F_stack: In t_stack 
        -- pré-condition : 
        -- post-condition : 
        -- exception : 
        -- test :   
        procedure print (F_stack : in t_stack) is
            iterator : t_stack ;
            begin
                iterator := F_stack;
                while iterator /= null loop
                    afficher_element(iterator.all.element);
                    iterator := iterator.all.suivant;
                end loop;
                new_line;
        end print;

        -- nom : pop
        -- sémantique : pop the first element of F_stack 
        -- paramètres : F_stack: In/Out liste 
        -- pré-condition : 
        -- post-condition : F_element n'appartion pas a F_stack
        -- exception : 
        -- test :   
        function pop (F_stack : in out t_stack) return T_generic is
            element : T_generic;
            begin
                -- si c'est vide on raise une exeption
                if is_empty(F_stack) then
                    raise empty_stack;
                else
                    element := F_stack.all.element;
                    -- On suprime l'element courant attention au cas null
                    if is_empty (F_stack.all.suivant) then
                        F_stack := create_empty_stack;
                    else
                        F_stack := F_stack.suivant;
                    end if;
                    return element;
                end if;
        end pop;
end p_pile;