-- Callune Gitenet SN APP 1A
-- utilise un package
-- with use block
with p_pile;

with text_io;
use text_io;

procedure calculatrice is
    -- instanciation de la gé&néricité 
    procedure Put_Int(e : in Integer) is
    begin
        Put(Integer'Image(e));
    end Put_Int;
    package p_pile_entier is new p_pile(T_generic => Integer, afficher_element => Put_Int);
    use p_pile_entier;
    -- constante declaration
    -- exception declaration
    zero_devide_exception: exception;
    -- types declaration
    -- procedure & function block
    
    procedure calculate is
      commande : String(1..10);
      longueur : Integer;

      stack : p_pile_entier.t_stack;
      nb_elements : Integer := 0; -- nombre d'éléments dans la stack

      operande1 : Integer;
      operande2 : Integer;
      resultat : Integer;
   begin
      stack := p_pile_entier.create_empty_stack;

      loop
         put("Veuillez saisir un opérande ou une opération ou (q)uitter: ");
         get_line(commande, longueur);

         if (commande(1..longueur) = "+" or commande(1..longueur) = "-" or
               commande(1..longueur) = "*" or commande(1..longueur) = "/")
         then

            if nb_elements < 2 then
               put("Pas assez d'éléments dans la pile !");
               new_line;
            else

               operande1 := p_pile_entier.pop(stack);
               operande2 := p_pile_entier.pop(stack);
               if commande(1..longueur) = "+" then
                  resultat := operande1 + operande2;
                  put(Integer'Image(operande1) & " + " & Integer'Image(operande2)& " = " & Integer'Image(resultat));
                  p_pile_entier.push(stack, resultat);
               elsif commande(1..longueur) = "-" then
                  resultat := operande1 - operande2;
                  put(Integer'Image(operande1) & " - " & Integer'Image(operande2)& " = " & Integer'Image(resultat));
                  p_pile_entier.push(stack, resultat);
               elsif commande(1..longueur) = "*" then
                  resultat := operande1 * operande2;
                  put(Integer'Image(operande1) & " * " & Integer'Image(operande2)& " = " & Integer'Image(resultat));
                  p_pile_entier.push(stack, resultat);
               elsif commande(1..longueur) = "/" then
                  if operande2 = 0 then
                     raise zero_devide_exception;
                  else
                     resultat := operande1 / operande2;
                     put(Integer'Image(operande1) & " / " & Integer'Image(operande2)& " = " & Integer'Image(resultat));
                     p_pile_entier.push(stack, resultat);
                  end if;
               end if;
               new_line;
               nb_elements := nb_elements - 1;
            end if;

         elsif commande(1..longueur) /= "q" and commande(1..longueur) /= "quitter" then
            p_pile_entier.push(stack, Integer'Value(commande(1..longueur)));
            nb_elements := nb_elements + 1;
         end if;

         exit when commande(1..longueur) = "q" or commande(1..longueur) = "quitter";
      end loop;
   exception
      when Constraint_Error =>
         put("Saisie invalide. La mémoire de la calculatrice est réinitialisée.");
         new_line;
         calculate;
      when zero_devide_exception => 
         put("Changez d'opération, la division se fait par 0.");
         new_line;
         calculate;
   end calculate;
    -- main's variable
    my_stack : t_stack;
    poped : integer;

begin 
  my_stack :=create_empty_stack;
  for i in 1..5 loop
    push(my_stack,i);
    print(my_stack);
  end loop;
  for i in reverse 1..5 loop
    poped := pop(my_stack);
    put_line("poped "&poped'image);
    print(my_stack);
  end loop; 

  calculate; 
end calculatrice;