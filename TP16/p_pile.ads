-- Callune Gitenet SN APP 1A
-- implementation d'une pile grace a une liste chainée d'T_generic
generic
    type T_generic is private; 
    with procedure afficher_element(element:in T_generic);
package p_pile is
    -- constante declaration
    -- exception declaration
    empty_stack :exception;
    -- types declaration
    Type t_cell_generic is private;
    type t_stack  is private;
    -- procedure & function block

        -- nom : create_empty_stack
        -- sémantique : create_empty_stack
        -- type de retour : t_stack
        -- paramètres : 
        -- pré-condition : 
        -- post-condition : isEmpty
        -- exception : 
        -- test :  

        function create_empty_stack return t_stack;

        -- nom : is_empty
        -- sémantique : is_empty
        -- type de retour : boolean
        -- paramètres :  F_stack : in t_stack
        -- pré-condition : 
        -- post-condition : vrai si is_empty
        -- exception : 
        -- test :  

        function is_empty (F_stack: in t_stack) return boolean;

        -- nom : push
        -- sémantique : stack on top 
        -- paramètres : F_stack: In/Out t_stack 
        --              F_nouveau : In T_generic 
        -- pré-condition : 
        -- post-condition : F_nouveau apartien F_stack
        -- exception : 
        -- test :   
        procedure push (F_stack : in out t_stack;F_nouveau : in T_generic );

        -- nom : print
        -- sémantique : print the stack
        -- paramètres : F_stack: In t_stack 
        -- pré-condition : 
        -- post-condition : 
        -- exception : 
        -- test :   
        procedure print (F_stack : in t_stack);

        -- nom : pop
        -- sémantique : pop the first element of F_stack 
        -- paramètres : F_stack: In/Out liste 
        -- pré-condition : 
        -- post-condition : F_element n'appartion pas a F_stack
        -- exception : 
        -- test :   
        function pop (F_stack : in out t_stack) return T_generic;

private 
    type t_stack  is access t_cell_generic;
    type t_cell_generic is record
            element : T_generic;
            suivant : t_stack;
    end record;

end p_pile;