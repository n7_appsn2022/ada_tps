package body P_AB is

   -- nom : initialiser
   -- sémantique :Initialiser un AB Abr. L'AB est vide.
   -- paramètres : abr: out T_AB
   -- pré-condition : 
   -- post-condition : abr = null
   -- exception : 
   procedure initialiser (abr : out T_AB) is
   begin
      abr := null;
   end initialiser;

   -- nom : est_vide
   -- return type : boolean
   -- sémantique : Est-ce qu'un AB abr est vide ?
   -- paramètres : abr: in T_AB
   -- pré-condition : 
   -- post-condition : 
   -- exception : 
   function est_vide (abr : in T_AB) return Boolean is
   begin
      return abr = null;
   end est_vide;

   -- nom : taille
   -- return type : Integer
   -- sémantique : Obtenir le nombre d'éléments d'un AB
   -- paramètres : abr: in T_AB
   -- pré-condition : 
   -- post-condition : 
   -- exception : 
   function taille (abr : in T_AB) return Integer is
      resultat : Integer := 0;

   begin
      if abr /= null then
         resultat := resultat + taille(abr.all.Sous_Arbre_Gauche);
         resultat := resultat + taille(abr.all.Sous_Arbre_Droit);
         resultat := resultat + 1;
      end if;

      return resultat;
   end taille;

   -- nom : inserer
   -- sémantique : Inserer la donnée dans l'AB abr.
   -- paramètres : abr: in/out T_AB
   --              donnee : in Un_Type
   -- pré-condition : 
   -- post-condition : 
   -- exception : 
   procedure inserer (abr : in out T_AB; donnee : in Un_Type) is
   begin
      if abr = null then
         abr := new T_Noeud'(donnee, null, null);

      elsif donnee < abr.all.Donnee then
         if abr.all.Sous_Arbre_Gauche = null then
            abr.all.Sous_Arbre_Gauche := new T_Noeud'(donnee, null, null);
         else
            inserer(abr.all.Sous_Arbre_Gauche, donnee);
         end if;

      elsif donnee > abr.all.Donnee then
         if abr.all.Sous_Arbre_Droit = null then
            abr.all.Sous_Arbre_Droit := new T_Noeud'(donnee, null, null);
         else
            inserer(abr.all.Sous_Arbre_Droit, donnee);
         end if;
      end if;
   end inserer;

   -- nom : recherche
   -- return type : Boolean
   -- sémantique : Recherche dans l'AB abr.
   -- paramètres : abr: in T_AB
   --              donnee : in Un_Type
   -- pré-condition : 
   -- post-condition : 
   -- exception : 
   function recherche(abr : in T_AB; donnee : in Un_Type) return Boolean is
      resultat : Boolean := False;

   begin
      if abr /= null then
         if abr.all.Donnee = donnee then
            resultat := True;

         elsif donnee < abr.all.Donnee then
            resultat := recherche(abr.all.Sous_Arbre_Gauche, donnee);

         elsif donnee > abr.all.Donnee then
            resultat := recherche(abr.all.Sous_Arbre_Droit, donnee);
         end if;
      end if;

      return resultat;
   end recherche;

   -- nom : modifier
   -- sémantique : Modifier la donnée dans l'AB abr.
   -- paramètres : abr: in/out T_AB
   --              src_donnee : in Un_Type
   --              tar_donnee : in Un_Type
   -- pré-condition : 
   -- post-condition : 
   -- exception : .
   procedure modifier (abr : in out T_AB; src_donnee : in Un_Type; tar_donnee : in Un_Type) is
   begin
      supprimer(abr, src_donnee);
      inserer(abr, tar_donnee);
   end modifier;

   -- nom : supprimer
   -- sémantique : Supprimer la donnée dans l'AB abr.
   -- paramètres : abr: in/out T_AB
   --              donnee : in Un_Type
   -- pré-condition : 
   -- post-condition : 
   -- exception : 
   procedure supprimer (abr : in out T_AB; donnee : in Un_Type) is
      parent : T_AB := null; -- parent du noeud � supprimer
      node : T_AB := abr; -- noeud � supprimer
      parent_successeur : T_AB; -- parent du successeur du noeud � supprimer
   begin
      if recherche(abr, donnee) then

         -- Recherche du parent de l'�l�ment � supprimer
         if abr.all.Donnee /= donnee then -- si l'�l�ment � supprimer n'est pas la racine
            parent := abr;
            while not ((parent.all.Sous_Arbre_Gauche /= null and then parent.all.Sous_Arbre_Gauche.all.Donnee = donnee) or
                       (parent.all.Sous_Arbre_Droit /= null and then parent.all.Sous_Arbre_Droit.all.Donnee = donnee)) loop
               if donnee < parent.all.Donnee then
                  parent := parent.all.Sous_Arbre_Gauche;
               else
                  parent := parent.all.Sous_Arbre_Droit;
               end if;
            end loop;

            -- Recherche de l'�l�ment � supprimer
            if donnee < parent.all.Donnee then
               node := parent.all.Sous_Arbre_Gauche;
            else
               node := parent.all.Sous_Arbre_Droit;
            end if;
         end if;

         -- Si l'�l�ment � supprimer n'a aucun fils
         if node.all.Sous_Arbre_Gauche = null and node.all.Sous_Arbre_Droit = null then
            if parent = null then
               abr := null;
            elsif node = parent.all.Sous_Arbre_Gauche then
               parent.all.Sous_Arbre_Gauche := null;
            else
               parent.all.Sous_Arbre_Droit := null;
            end if;

         -- Si l'�l�ment � supprimer poss�de 1 fils
         elsif (node.all.Sous_Arbre_Gauche = null and node.all.Sous_Arbre_Droit /= null) or (node.all.Sous_Arbre_Gauche /= null and node.all.Sous_Arbre_Droit = null) then
            if parent = null then -- si racine
               if node.all.Sous_Arbre_Gauche /= null then
                  abr := node.all.Sous_Arbre_Gauche;
               else
                  abr := node.all.Sous_Arbre_Droit;
               end if;

            elsif node = parent.all.Sous_Arbre_Gauche then -- si pas racine et que le fils se trouve � gauche
               if node.all.Sous_Arbre_Gauche /= null then
                  parent.Sous_Arbre_Gauche := node.all.Sous_Arbre_Gauche;
               else
                  parent.Sous_Arbre_Gauche := node.all.Sous_Arbre_Droit;
               end if;

            else -- si pas racine et que le fils se trouve � droite (seule autre possibilit�)
               if node.all.Sous_Arbre_Gauche /= null then
                  parent.Sous_Arbre_Droit := node.all.Sous_Arbre_Gauche;
               else
                  parent.Sous_Arbre_Droit := node.all.Sous_Arbre_Droit;
               end if;
            end if;

         -- Si l'�l�ment � supprimer poss�de 2 fils
         else
            -- On commence par r�cup�rer le successeur de l'�l�ment � supprimer (la plus petite valeur du sous-arbre � droite de l'�l�ment � supprimer)
            parent_successeur := node;
            if node.all.Sous_Arbre_Droit.all.Sous_Arbre_Gauche /= null then
               parent_successeur := node.all.Sous_Arbre_Droit;

               while parent_successeur.all.Sous_Arbre_Gauche.all.Sous_Arbre_Gauche /= null loop
                  parent_successeur := parent_successeur.all.Sous_Arbre_Gauche;
               end loop;
            end if;

            -- On remplace la valeur du noeud � supprimer par celle du successeur
            if node = parent_successeur then
               node.all.Donnee := parent_successeur.all.Sous_Arbre_Droit.all.Donnee;
               parent_successeur.all.Sous_Arbre_Droit := parent_successeur.all.Sous_Arbre_Droit.all.Sous_Arbre_Droit;
            else
               node.all.Donnee := parent_successeur.all.Sous_Arbre_Gauche.all.Donnee;
               parent_successeur.all.Sous_Arbre_Gauche := parent_successeur.all.Sous_Arbre_Gauche.all.Sous_Arbre_Droit;
            end if;

         end if;
      end if;
   end supprimer;

   -- nom : afficher
   -- sémantique : Afficher un AB abr.
   -- paramètres : abr: in T_AB
   -- pré-condition : 
   -- post-condition : 
   -- exception : 
   procedure afficher(abr : in T_AB) is
   begin
      if abr /= null then
         Put(abr.all.Donnee);
         Put(" ");
         afficher(abr.all.Sous_Arbre_Gauche);
         afficher(abr.all.Sous_Arbre_Droit);
      end if;
   end afficher;
end P_AB;
