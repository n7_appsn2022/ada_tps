with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io ;
generic
   type Un_Type is private; 
   with function "=" (Left, Right : Un_Type) return Boolean is <>;
   with function ">" (Left, Right : Un_Type) return Boolean is <>;
   with function "<" (Left, Right : Un_Type) return Boolean is <>;
   with procedure Put(e : Un_Type);

package P_AB is

   type T_AB is private;
   -- nom : initialiser
   -- sémantique :Initialiser un AB Abr. L'AB est vide.
   -- paramètres : abr: out T_AB
   -- pré-condition : 
   -- post-condition : abr = null
   -- exception : 
   procedure initialiser (abr : out T_AB) ;

   -- nom : est_vide
   -- return type : boolean
   -- sémantique : Est-ce qu'un AB abr est vide ?
   -- paramètres : abr: in T_AB
   -- pré-condition : 
   -- post-condition : 
   -- exception : 
   function est_vide (abr : in T_AB) return Boolean ;

   -- nom : taille
   -- return type : Integer
   -- sémantique : Obtenir le nombre d'éléments d'un AB
   -- paramètres : abr: in T_AB
   -- pré-condition : 
   -- post-condition : 
   -- exception : 
   function taille (abr : in T_AB) return Integer ;

   -- nom : inserer
   -- sémantique : Inserer la donnée dans l'AB abr.
   -- paramètres : abr: in/out T_AB
   --              donnee : in Un_Type
   -- pré-condition : 
   -- post-condition : 
   -- exception : 
   procedure inserer (abr : in out T_AB; donnee : in Un_Type) ;

   -- nom : recherche
   -- return type : Boolean
   -- sémantique : Recherche dans l'AB abr.
   -- paramètres : abr: in T_AB
   --              donnee : in Un_Type
   -- pré-condition : 
   -- post-condition : 
   -- exception : 
   function recherche(abr : in T_AB; donnee : in Un_Type) return Boolean ;

   -- nom : modifier
   -- sémantique : Modifier la donnée dans l'AB abr.
   -- paramètres : abr: in/out T_AB
   --              src_donnee : in Un_Type
   --              tar_donnee : in Un_Type
   -- pré-condition : 
   -- post-condition : 
   -- exception : 
   procedure modifier (abr : in out T_AB; src_donnee : in Un_Type; tar_donnee : in Un_Type);

   -- nom : supprimer
   -- sémantique : Supprimer la donnée dans l'AB abr.
   -- paramètres : abr: in/out T_AB
   --              donnee : in Un_Type
   -- pré-condition : 
   -- post-condition : 
   -- exception : 
   procedure supprimer (abr : in out T_AB; donnee : in Un_Type);

   -- nom : afficher
   -- sémantique : Afficher un AB abr.
   -- paramètres : abr: in T_AB
   -- pré-condition : 
   -- post-condition : 
   -- exception : 
   procedure afficher(abr : in T_AB) ;

   private
   type T_Noeud;
   type T_AB is access T_Noeud;
   Type T_Noeud is 
      record
         Donnee: Un_Type;
         Sous_Arbre_Gauche : T_AB;
         Sous_Arbre_Droit : T_AB;
      end record;
   
end P_AB;