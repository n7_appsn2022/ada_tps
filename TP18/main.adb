-- Callune Gitenet SN APP 1A
-- main qui test l'arbre binaire de recherche
with p_ab;

with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io ;

procedure main is
    procedure Put(e: Integer) is 
    begin 
        Put(Integer'image(e));
    end Put;
    package P_AB_Entier is new p_ab(Integer,"=","<",">",Put);
    use P_AB_Entier;

    my_tree : T_AB;
begin
    initialiser(my_tree);
    for i in 1..10 loop
        inserer(my_tree,i);
    end loop;
    afficher(my_tree);
end main;