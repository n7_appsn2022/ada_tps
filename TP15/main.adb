-- Callune Gitenet SN APP 1A
-- utilise un package
-- with use block
with p_double_liste;

with text_io;
use text_io;



procedure main is
    procedure Put_Int(e : in Integer) is
    begin
        Put(Integer'Image(e));
    end Put_Int;
    
    function inferior_or_equals(gauche :in Integer; droite : in Integer) return boolean is
    begin
        return gauche <= droite;
    end inferior_or_equals;

    function equals(gauche :in Integer; droite : in Integer) return boolean is
    begin
        return gauche = droite;
    end equals;

    package p_liste_entier is new p_double_liste(T_generic => Integer, afficher_element => Put_Int, inferior_or_equals => inferior_or_equals, equals => equals);
    use p_liste_entier;

    -- constante declaration
    -- exception declaration
    -- types declaration
    -- procedure & function block
    -- main's variable
    my_list_int : p_liste_entier.t_liste;

begin 
    my_list_int := creer_liste_vide;
    for i in 5..7 loop
        ajouter(my_list_int,i);
        afficher_liste(my_list_int);
    end loop;
    for i in reverse 1..4  loop
        ajouter(my_list_int,i);
        afficher_liste(my_list_int);
    end loop;
    enlever(my_list_int,3);
    afficher_liste(my_list_int);

end main;