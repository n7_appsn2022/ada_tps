-- Callune Gitenet SN APP 1A
-- implementation d'une liste chainée d'T_generic
-- with use block
with text_io;
use text_io;

package body  p_double_liste is
-- procedure & function block

    -- nom : créer_liste_vide
    -- sémantique : créer_liste_vide
    -- type de retour : t_liste
    -- paramètres : 
    -- pré-condition : 
    -- post-condition : est vide
    -- exception : 
    -- test :  

    function creer_liste_vide return t_liste is
    --déclaration variables locales
    new_liste :t_liste;
    begin
        new_liste := null;
        return new_liste;
    end creer_liste_vide;

    -- nom : est_vide
    -- sémantique : est_vide
    -- type de retour : boolean
    -- paramètres :  F_liste : in t_liste
    -- pré-condition : 
    -- post-condition : est vide
    -- exception : 
    -- test :  

    function est_vide (F_liste: in t_liste) return boolean is
    --déclaration variables locales
    begin
        return F_liste = null;
    end est_vide;

    -- nom :ajouter
    -- sémantique : ajouter
    -- paramètres : F_liste: In/Out liste 
    --              F_nouveau : In T_generic 
    -- pré-condition : 
    -- post-condition : F_nouveau apartien F_liste
    -- exception : 
    -- test :   
    procedure ajouter (
        F_liste : in out t_liste;
        F_nouveau : in T_generic ) is
    -- déclaration variables locales
    newliste : t_liste;
    
    begin
        if est_vide(F_liste) then
            F_liste := new t_cell_elem;
            F_liste.all.element := F_nouveau;
            F_liste.all.suivant := null;
            F_liste.all.precedent := null;

        else
            if inferior_or_equals(F_nouveau, F_liste.all.element) then
                -- R1 on cherche a inserer avant 
                while F_liste.all.precedent /= null and then inferior_or_equals(F_nouveau, F_liste.all.precedent.all.element) loop
                    F_liste := F_liste.all.precedent;
                end loop;
                -- on insert
                newliste := new t_cell_elem;
                newliste.all.element := F_nouveau;
                newliste.all.suivant := F_liste;
                newliste.all.precedent := F_liste.all.precedent;
                F_liste := newliste;
            else
                -- R1 on cherche a inserer aprés
                while F_liste.all.suivant /= null and then not inferior_or_equals(F_nouveau, F_liste.all.element) loop
                    F_liste := F_liste.all.suivant;
                end loop;
                newliste := new t_cell_elem;
                newliste.all.element := F_nouveau;
                newliste.all.suivant := F_liste.all.suivant;
                newliste.all.precedent := F_liste;
                if F_liste.all.suivant /= null then
                    F_liste.all.suivant.all.precedent := newliste;
                end if;
                F_liste.all.suivant := newliste;
                F_liste := F_liste.all.suivant;
            end if;
        end if;
    end ajouter;

    -- nom :afficher_liste
    -- sémantique : afficher_liste
    -- paramètres : F_liste: In/Out liste 
    -- pré-condition : 
    -- post-condition : 
    -- exception : 
    -- test :   
    procedure afficher_liste (F_liste : in t_liste) is
    -- déclaration variables locales
    iterator : t_liste;
    begin
        iterator := F_liste;
        if not est_vide(iterator) then
            while iterator.all.precedent /= null loop
                iterator := iterator.all.precedent;
            end loop;
            while iterator /= null loop
                afficher_element(iterator.all.element);
                iterator := iterator.all.suivant;
            end loop;
        end if;
        new_line;
    end afficher_liste;

    -- nom : rechercher
    -- sémantique : rechercher un element dans la liste retourne son adresse ou null
    -- type de retour : t_liste
    -- paramètres :  F_liste : in t_liste
    --               F_element : in T_generic
    -- pré-condition : 
    -- post-condition : 
    -- exception : 
    -- test :  

    function rechercher (F_liste: in out t_liste; F_element :in T_generic) return t_liste is
    --déclaration variables locales
    trouve : boolean;
    iterator : t_liste;
    begin
        trouve := false;
        iterator := F_liste;
        if inferior_or_equals(F_element,iterator.all.element) then
            while iterator /= null and not trouve loop 
                if equals(F_element, iterator.all.element) then
                    trouve := true;
                    F_liste := iterator;
                else
                    iterator := iterator.all.precedent;
                end if;
            end loop;
        else
            while iterator /= null and not trouve loop 
                if equals(F_element, iterator.all.element) then
                    trouve := true;
                    F_liste := iterator;
                else
                    iterator := iterator.all.suivant;
                end if;
                
            end loop;
        end if;
        if trouve then
            return iterator;
        else 
            raise not_found;
        end if;
    end rechercher;


    -- nom :enlever
    -- sémantique : enlever F_element de F_liste 
    -- paramètres : F_liste: In/Out liste 
    --              F_element : in T_generic
    -- pré-condition : 
    -- post-condition : F_element n'appartion pas a F_liste
    -- exception : 
    -- test :   
    procedure enlever (F_liste : in out t_liste; F_element:in T_generic) is
    -- déclaration variables locales
    to_be_removed : t_liste;
    begin
        to_be_removed := rechercher(F_liste,F_element);
        -- on suprime le lien suivant du précédent qui chaine sur nous si il existe
        if to_be_removed.all.precedent /= null then
            to_be_removed.all.precedent.all.suivant := to_be_removed.all.suivant;
            F_liste := to_be_removed.all.precedent;
        else
            F_liste := to_be_removed.all.suivant;
        end if;

        -- On suprime le lien precedent du suivant qui chaine sur nour si il existe
        if to_be_removed.all.suivant /= null then
            to_be_removed.all.suivant.all.precedent := to_be_removed.all.precedent;
        end if;
    end enlever;
end p_double_liste;