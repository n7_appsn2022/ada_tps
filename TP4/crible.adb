-- Callune Gitenet SN APP 1A
-- définit les nombres premiers de 2 a N par la méthode du crible d'eratosthene
-- précondition 
-- postcondition :
-- test :

-- with use block
with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io ;

procedure crible is

-- constante declaration
capacite_n : constant INTEGER := 8190;
-- types declaration
type t_prime is (PRIME, NOTPRIME);
type t_arr_prime is array (0 .. capacite_n-2 ) of t_prime;
-- procedure & function block
-- main's variable
arr_sorted_numbers : t_arr_prime;
n : INTEGER;  -- con cherchera les entier entre 2 et n
i : INTEGER;


begin 
-- R1.2 : L'utilisateur saisit un nombre supérieure a 2
	loop
        put_line("Saisir n > 2 et < " & Integer'Image(capacite_n));
        Get(n);
    exit when n>2 and n<capacite_n;
    end loop;

--R1.2 on  initialise le tableau 
    for init in 1..n loop
        arr_sorted_numbers(init):= PRIME;
    end loop;
--R1.3 : On s'arrete lorsqu'il n'y a plus de nombre premier 
    i := 2 ;
    while i*i < n  loop
        --R2 pour chaque  chiffre, on vérifie si il a été éliminé ou pas
        if arr_sorted_numbers(i) /=  NOTPRIME then
            --R3 Si il est premier, on élimine tout ses multiples
            for set_not_prime_iterator in i..n loop
                if set_not_prime_iterator /= i and set_not_prime_iterator mod i = 0 then
                    arr_sorted_numbers(set_not_prime_iterator) := NOTPRIME;
                else
                    null;
                end if;
            end loop;
        else
            null;
        end if;
        i := i +1 ;
    end loop;
--R1.4 afficher le tableau  
    Put("[");
    -- On parcours le tableau
    for i in 1..n loop
        if arr_sorted_numbers(i) = PRIME then
            -- on affiche l'element
            Put(Integer'Image(i));
            -- on met une virgule si ce n'est pas fini
            if i /= n then
                Put(",");
            else 
                null;
            end if;
        else
            null;
        end if;
        
    end loop;
    Put("]");
end crible;