-- Callune Gitenet SN APP 1A
-- Pour E/S sur les chaines de caractère
with text_io;
use text_io;

-- Pour E/S sur les entiers
with ada.integer_text_io;
use ada.integer_text_io;

-- R0 : Trier par insertion les éléments d'un tableau puis afficher le tableau trié
--      Les éléments du tableau sont des entiers
procedure tri_par_insertion is
-- Pre-condition : RAS
-- Post-condition : Le tableau est trié et affiché

    type T_Tab is array(Integer range <>) of Integer;       -- Tableau d'entiers

    tab : T_Tab(0..9) := (7, 5, 3, 17, 12, 4, 8, 11, 13, 6);   -- Tableau à trier
    nb : Integer;       -- Pour mémorisation du nombre contenu à un certain indice
    j : Integer;        -- Variable utilisée pour le décalage d'éléments supérieurs à nb

begin

    -- R1_1 : Afficher le tableau avant le tri
    put("Table before sorting : (");
    for i in tab'range loop
        put(integer'image(tab(i)));
    end loop;
    put_line(" )");

    -- R1_2 : Parcours de la totalité du tableau pour le trier
    for i in tab'range loop
        -- R2_1 : Mémorisation du nombre contenu à l'indice i du tableau, soit tab(i)
        nb := tab(i);
        -- R2_2 : Décalage vers la droite des éléments supérieurs à ce même nombre
        j := i;
        while j > 0 and then tab(j-1) > nb loop
            tab(j) := tab(j-1);
            j := j-1;
        end loop;
        -- R2_3 : Mettre le nombre qui était autrefois à l'indice i à la place qu'il faut
        tab(j) := nb;
    end loop;

    -- R1_3 : Afficher le nouveau tableau après le tri
    put("Table after sorting : (");
    for i in tab'range loop
        put(integer'image(tab(i)));
    end loop;
    put_line(" )");

end tri_par_insertion;