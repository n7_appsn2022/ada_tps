-- Callune Gitenet SN APP 1A
-- insere l'element en premiere case du tableau 
-- précondition : reste de la place dans le tableau
-- postcondition : nb_elements s'incrémente de 1 , tab 1 = element, tab(2) = tab'avant(1)
-- test :  (1,3,4),val =4 => (4,1,3,4) 
-- (1,3,4,4,4),val =5 =>  "insertion impossible" 

-- with use block
with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io;

procedure inserer_ada is

-- constante declaration
NMAX : constant INTEGER := 5; -- Indice maximum du tableau 
-- types declaration
TYPE TAB_ENTIERS is ARRAY(1..NMAX) of INTEGER;
-- procedure & function block
-- main's variable
un_tab :TAB_ENTIERS; -- un tableau d'au maximum NMAX entier
nb_elements : INTEGER; -- le nombre effectif d'élément (<=NMAX) de un_tab.
val : INTEGER;  -- la valeur recherchée 

begin 
-- R1.1 on itialise le tableau 
    un_tab := (5,6,2,0,5);
    nb_elements := 4;
-- R1.2 si le tableau est plein 
    if nb_elements = NMAX then
        Put_line("Insertion impossible");
    else
-- R1.3 saisir val
        Put_line("Saisir la valeur à rechercher dans le tableau :");
        Get(val);
-- R1.4 décaler a droite tout les élements
        for i in nb_elements..2 loop
            un_tab(i) := un_tab(i-1);
        end loop;
-- R1.5 inserer en case 1 val
        un_tab(1):= val;
-- R1.6 mettre a jour le nb_element
        nb_elements := nb_elements+1;
    end if;

    
end inserer_ada;