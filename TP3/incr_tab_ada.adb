-- Callune Gitenet SN APP 1A
-- ajoute 1 a chaque element du tableau
-- précondition : 
-- postcondition : Tout les éléments sont incrémenté de 1 , leurs nombres reste inchanger
-- test :  (1,3,4) => (2,4,5)

-- with use block
with text_io;
use text_io;

procedure incr_tab_ada is

-- constante declaration
NMAX : constant INTEGER := 5; -- Indice maximum du tableau 
-- types declaration
TYPE TAB_ENTIERS is ARRAY(1..NMAX) of INTEGER;
-- procedure & function block
-- main's variable
un_tab :TAB_ENTIERS; -- un tableau d'au maximum NMAX entier
nb_elements : INTEGER ; -- le nombre effectif d'élément (<=NMAX) de un_tab.


begin 
-- R1.1 on itialise le tableau 
    un_tab := (5,6,2,0,0);
    nb_elements := 5;
-- R1.2 on incrémente les élément du tableau
    for element of un_tab loop
        element := element +1;
    end loop;

end incr_tab_ada;