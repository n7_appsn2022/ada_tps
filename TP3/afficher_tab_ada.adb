-- Callune Gitenet SN APP 1A
-- R0 affiche un tableau 
-- précondition : nb_elements >=0 
-- postcondition : affiche le tableau entre crochet
-- test :  [0] ,  [0,1,5]

-- with use block
with text_io;
use text_io;

procedure afficher_tab_ada is

-- constante declaration
NMAX : constant INTEGER := 5; -- Indice maximum du tableau 
-- types declaration
TYPE TAB_ENTIERS is ARRAY(1..NMAX) of INTEGER;
-- procedure & function block
-- main's variable
un_tab :TAB_ENTIERS; -- un tableau d'au maximum NMAX entier
nb_elements : INTEGER ; -- le nombre effectif d'élément (<=NMAX) de un_tab.

begin 
-- on itialise le tableau 
un_tab := (5,6,2,0,0);
nb_elements := 3;
 
Put("[");
-- On parcours le tableau
for i in 1..nb_elements loop
    -- on affiche l'element
    Put(Integer'Image(un_tab(i)));
    -- on met une virgule si ce n'est pas fini
    if i /= nb_elements then
        Put(",");
    else 
        null;
    end if;
end loop;
Put("]");

end afficher_tab_ada;
