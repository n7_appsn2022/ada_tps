-- Callune Gitenet SN APP 1A
-- trouve l'indice le la P-ieme itération de val
-- précondition : 0<p<nb_elements  
-- postcondition : renvoit l'indice de la p-ieme itération de val ou "valeur non présente" 
-- test :  (1,3,4,4,4),p =2, val =4 =>4  
-- (1,3,4,4,4),p =2, val =5 =>  "valeur non présente" 

-- with use block
with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io;

procedure foun_val_ada is

-- constante declaration
NMAX : constant INTEGER := 5; -- Indice maximum du tableau 
-- types declaration
TYPE TAB_ENTIERS is ARRAY(1..NMAX) of INTEGER;
-- procedure & function block
-- main's variable
un_tab :TAB_ENTIERS; -- un tableau d'au maximum NMAX entier
nb_elements : INTEGER; -- le nombre effectif d'élément (<=NMAX) de un_tab.
p : INTEGER; -- le nombre d'aparition souhaiter
val : INTEGER;  -- la valeur recherchée 
nb_apparition :INTEGER; -- nombre d'apparition effective de val dans un tab
i :INTEGER; -- iterateur du while
index : INTEGER; -- l'index de la p-ieme apparition de val dans un_tab

begin 
-- R1.1 on itialise le tableau 
    un_tab := (5,6,2,0,5);
    nb_elements := 5;

-- R1.2 saisir val
    Put_line("Saisir la valeur à rechercher dans le tableau :");
    Get(val);
-- R1.3 saisir p
    loop
        Put_line("Quel apparition de "& Integer'Image(val)&" rechercher ? >0  et <"& Integer'Image(nb_elements)&":");
        Get(p);
    exit when p>0 and p< nb_elements;
    end loop;
-- R1.4 on cherche dans le tableau chaque itération
    nb_apparition := 0;
    i := 1;
    while i<nb_elements and nb_apparition /= p loop
        if un_tab(i)=val then
            nb_apparition := nb_apparition +1;
            if nb_apparition = p then
                index:= i;
            else
                null;
            end if;
        else
            null;
        end if;
        i:= i+1;
    end loop;
-- R2.1 de R1.4 on gère le cas ou l'element n'est pas dans le tableau
    if nb_apparition >0 and nb_apparition = p then
        Put_line("La "&Integer'Image(p)&"-ieme occurence de "&Integer'Image(val)&" dans un_tab se trouve a l'index "&Integer'Image(index));
    else
        Put_line("valeur non présente");
    end if;
end foun_val_ada;