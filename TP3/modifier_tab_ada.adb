-- Callune Gitenet SN APP 1A
-- modifie le i-iéme element d'un tableau
-- précondition : 0<i< nb_elements
-- postcondition : tab(i) = saisi, le reste est inchangé
-- test : (5,6,2) , val = 23 , index = 2 => (5,23,6)

-- with use block
with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io;


procedure modifier_tab_ada is

-- constante declaration
NMAX : constant INTEGER := 5; -- Indice maximum du tableau 
-- types declaration
TYPE TAB_ENTIERS is ARRAY(1..NMAX) of INTEGER;
-- procedure & function block
-- main's variable
un_tab :TAB_ENTIERS; -- un tableau d'au maximum NMAX entier
nb_elements : INTEGER ; -- le nombre effectif d'élément (<=NMAX) de un_tab.
valeur : INTEGER ; -- valeur a insérer
index : INTEGER ; -- place ou insérer la valeur

begin 
-- R1.1 on itialise le tableau 
    un_tab := (5,6,2,0,0);
    nb_elements := 5;
-- R1.2 on saisie  la valeur a inséré   OUT valeur
    Put_line("Saisir la valeur à inserer dans le tableau :");
    Get(valeur);
-- R1.2 on saisie  l'index ou inséré la valeur OUT index
    loop
        Put_line("Ou inserer la valeur ? >0 & <"& Integer'Image(nb_elements)&":");
        Get(index);
    exit when index>0 and index< nb_elements;
    end loop;

-- R1.3 on insere la valeur
    un_tab(index) :=  valeur;

end modifier_tab_ada;
