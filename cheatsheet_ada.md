# Rappel syntaxique d'ADA
## Commandes 
Installer le paquet `sudo apt install gnatmake`  
Compiler `gnatmake -gnatwa myFile.adb`
Nettoyer `gnatclean myFile`  
## Erreur à la con
* On compte à partir de 1.
* L'accesseur au tableau est (i) et non [i]
* Opérateur de concaténation &
* Typage fort int + float non permis
* Les packages en lower case
### Mot-clé reservés 

        and  
        or
        not
        begin
        end
        mod
        do
        of
        for
        in
        reverse
        loop
        while
        until
        if
        then
        else
        elsif
        case
        when
        null
        constant
        is
        type
        array
        record
        function
        procedure
        return
        in
        out
        with
        use
        others
## Structure de contrôle
### For
    for <iterator name> in <1..n | tab'range> loop
        <intruction>
    end loop;
### Repeter jusqu'à
    loop
            <instruction>
    exit when <condition>;
    end loop;
### While
    while <condition> loop
            <instruction>
    end loop;
### If
    if <condition> then
        <instruction>  
    elsif <condition> then
        <instruction>    
    else 
        <instruction>      
    end if;
### Case
    case <Expression> is
        when <valeur> => <Instructions> ;
        when <valeur> => <Instructions> ;
        when <valeur> => <Instructions> ;
        when others   => <Instructions> ;
    end case;
## Elements de base
### Opérateurs
#### Unaire

    + - not
#### Binaire

    + - * / mod rem **
    > >= = < <=
    /= (ça c'est le not equals)
    or and
### Type

    Short_Integer
    Integer
    Long_Integer
    Float
    Long_Float
    Boolean
    Character
### Déclaration et initialisation
Pour une constante avec initialisation: `<var_name> : constant <Type> := <value> ; `

Pour une déclaration: `<var_name> : <Type> ;`

Pour une initialisation/affectation: `<var_name> := <value>;`
### Manipulation du type String et Character
TO_DO
Voir tp 5... 
### Manipulation du type Enum
`TYPE T_<type_name> is(<VALUES>,<VALUE>);`  
`<Type_enum_name>'Val(<int>)` : retourne la valeur a l'index.  
`<Type_enum_name>'Value(<string>)` : retourne l'index de la string.  
On peut itérer sur une enum avec un `̀for i in <enum> loop`.
`<enum>'Image`return surement qqch de printable.   
### Tableau
    type <t_name> is array (1..<MAX_CONST>, 1..<MAX_CONST>) of <content_type> ;
    type <t_name> is array (0..<MAX_CONST>) of integer range 0..1 ;
### Type records
    type <t_name> is record
            <var_name> : <var_type>;
            <var_name> : <var_type>;
    end record;
Acceceur : <t_name>.<var_name>
### Fonctions usuels

`Skip_Line;` `New_Line;`  
`Put(<expression>);` `Put_Line(<expression>);`
`Get(<variable>);` `Get_Line(<variable>);`
### Pointeur
    Type <t_type_name> is access <t_type_accessed>;
    <ptr_name> : <t_type_name> := null;
    -- Aprés le begin
    <ptr_name> := new <t_type_accessed>'(<constructor_param>);
    <ptr_name> := new <t_type_accessed>;
    <ptr_name>.all := <value>;
    <ptr_name>.all.<field> := <value>;
#### Exemple d'une liste chainer d'entier 
    Type t_cellule_integer;
    type t_liste_entier  is access t_cellule_integer;
    type t_cellule_integer is record
            element : entier;
            suivant : t_liste_entier;
    end record;


### Exception
#### Déclaration
`<nom_exception> : EXCEPTION;`
#### Déclenchement
`raise <nom_exeption>;`
#### Traitement
Entre le bégin et le end, on peut mettre un bloc "exception".
    nom : begin
    exception
        when <nom_exeption> => <instructions>;
    end nom;
#### Exception connu
`Constraint_error` lorsqu'un élément de type numérique ou non numérique prend une valeur en dehors d'un intervalle autorisé.  
`Data_error` erreurs issues de la manipulation de numérique ou issues de typage.  
`Storage_error` erreurs liées au manque de mémoire.  
`Status_error`erreurs liées à la manipulation de fichiers.  
## Module et paquetage
### Interface 
Dans un fichier .ads.
```Ada
package <P_name> is 
    -- annonce des types
    type <T_name> is private;
    -- anonce des procédures et fonctions
    procedure saisir (<F_name> :out <T_name>);
    function get (<F_name> :int <T_name>) return <T_retour>;

    -- partie privée non accessible de l'utilisateur
private
    type <T_name> is record
        1:type;
        2:type;
    end record;
end <P_name>;
```
### Corps
Dans un fichier .adb .
```Ada
package body <P_name> is 
    -- anonce des procédures et fonctions
    procedure saisir (<F_name> :out <T_name>)is 
        ...
    end saisir;
    function get (<F_name> :int <T_name>) return <T_retour> is
        ...
    end get;
end <P_name>;
```
### Utiliser un package
```Ada
with <P_name>;
use <P_name>;
procedure main is
    var :<T_du_paquetage>;
begin
    <fonction_du_paquetage>;
end main;

```
### With use utile

with text_io;
use text_io;

with ada.integer_text_io;
use ada.integer_text_io ;

### Généricité
```ada
-- Dans l'ads mettre `generic` au début ? 
-- Déclarer un type private et l'utiliser.
-- c'est en quelque sorte les paramètres du package
generic 
    type <Type_generic> is private;

package
procedure ... <Type_generic>... ;
-- Dans l'adb, utiliser le type generic private comme si de rien n'était.
procedure ...<Type_generic>...  is ...
-- Dans le programme principale qui utilisera ce type générique faire un with.  
-- Mais au lieu de faire un use, il faut instancier le paquet.  
with <P_name>;
procedure main is
    package <P_name_entier> is new <P_name> (<Type_generic> => <Integer>);
    use <P_name_entier>;
begin 
end main;
```